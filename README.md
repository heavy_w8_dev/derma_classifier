Pure vision-driven classification of dermoscopic images among nine different diagnostic categories without using the meta-data information concerning nevus location, patient background info or other.
[Original ISIC-2019 dataset](https://www.kaggle.com/andrewmvd/isic-2019)


## <div align="left">Input Data and EDA (Exploratory Data Analysis).</div>
25,331 images are available for training across 8 different categories:

| anatom_site_general | MEL  | NV   | BCC  | AK  | BKL  | DF  | VASC | SCC | UNK |
|---------------------|------|------|------|-----|------|-----|------|-----|-----|
| anterior torso      | 1331 | 3699 | 1114 | 92  | 445  | 43  | 78   | 113 | 0   |
| head/neck           | 880  | 767  | 1131 | 598 | 1001 | 0   | 36   | 174 | 0   |
| lateral torso       | 14   | 34   | 0    | 0   | 6    | 0   | 0    | 0   | 0   |
| lower extremity     | 796  | 2876 | 462  | 66  | 412  | 138 | 58   | 182 | 0   |
| oral/genital        | 19   | 24   | 0    | 0   | 14   | 0   | 2    | 0   | 0   |
| palms/soles         | 201  | 168  | 0    | 6   | 7    | 0   | 3    | 13  | 0   |
| posterior torso     | 430  | 1888 | 186  | 4   | 230  | 2   | 22   | 25  | 0   |
| upper extremity     | 724  | 1325 | 358  | 79  | 234  | 52  | 23   | 115 | 0   |


<details open>
    <summary>Initial metastudy to estimate bias towards gender, age, nevus location, etc.[summary statistics and graphical representations](https://gitlab.com/heavy_w8_dev/derma_classifier/-/blob/master/eda.ipynb)</summary>
    <details>
        <summary>Nevus type distribution</summary>
        ![nevus type distribution violin plot ](/uploads/32eff819d994dfc36b0ec1a5153c213d/image.png)
    </details>
    <details>
        <summary>Nevus location distribution</summary>
        ![nevus location distribution violin plot](/uploads/b626d0dce883a61c52a090a09f475b80/image.png)
    </details>
</details>


## <div align="left">Project worthiness and NEATOs</div>
* Visuals-prioritized dataset metastudy in jupy-notebook
* Model independent performance tracker for classification task
* GPU utilization profiling to detect data leaks, bottlenecks and study scaling potential
* Dataset evaluator to estimate mean and std of the input data - for tailored augmentation strategies 
* Dataset preparator - to perform common preprocessing step and avoid CPU-bound repeated operations
* Scalable ensemble-level logging with fixed input (for faster evaluation and training)


## <div align="left">Preprocessing</div>
As an initial step several task-specific preprocessing stages were performed to build a new dataset in order to avoid CPU-bound repeated operations.Those stages included lenses / dark zones cropping and Shades of Gray color consistency.
[manuscript from last year's winners](https://isic-challenge-stade.s3.amazonaws.com/99bdfa5c-4b6b-4c3c-94c0-f614e6a05bc4/method_description.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA2FPBP3IISBDRZKPE%2F20210716%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210716T123305Z&X-Amz-Expires=21600&X-Amz-SignedHeaders=host&X-Amz-Signature=74741bf7bf53bd91e5f2e173a2bc696a155c32518adfab4dd5be58a3b2d6222c)


## <div align="left">Model performance is tracked on the fly (per epoch) on Validation set</div>

<details open>
    <summary>Accuracy, Scores and Loss (mean over validation set) </summary>
    <img src="/git_images/validation_metrics.png">
</details>

<details>
    <summary>Sample predictions are made on the fixed inputs to monitor the learning progress</summary>
    <img src="/git_images/sample_prediction.png" width="1000" height="700">
    <img src="/git_images/predictions_grid.png">
</details>

<details>
    <summary>Per class precision</summary>
    <img src="/git_images/avg_precision_grid.png">
</details>

<details>
    <summary>Confusion matrix</summary>
    ![conf_mat_progress](/uploads/881b1e6bc68c2216731833430bebc99d/conf_mat_progress.webm)
    <img src="/git_images/conf_mat_grid.png">
</details>

<details>
    <summary>Receiver Operating Characteristic (ROC)</summary>
    ![roc_progress](/uploads/21fc55502c3a31a0c6478d1f34c23f5f/roc_progress.webm)
    <img src="/git_images/roc_plots_grid.png" width="1830" height="1275">
</details>

<details>
    <summary>Area under Receiver Operating Characteristic (ROC AUC)</summary>
    <img src="/git_images/roc_grid.png">
</details>


## <div align="left">Trained convolutional networks interpretation</div>
The lack of transparency of decision process inside DNN makes them diffucult to adapt in many critical fields, especially medical domain. In order earn users trus it is required to cast light and make the process less obscure.
By using model interpretability framework such as [captum](https://github.com/pytorch/captum) it is possible to utilize multiple techniques and study the model behaviour.
In ideal scenario each independent network in the ensemble should learn to operate on the same visual patterns as trained dermatologists.

<details open>
    <summary>Most frequerent patterns encountered during visual examination of nevi presented at [journal Journal of Osteopathic Medicine](https://doi.org/10.7556/jaoa.2019.067)</summary>
    <img src="/uploads/940c36a3d5a87d22b4302d1bd0c000fb/image.png">
    Image by Nadeem G. Marghoob, Konstantinos Liopyris and Natalia Jaimes, [journal Journal of Osteopathic Medicine](https://doi.org/10.7556/jaoa.2019.067), Dermoscopy: A Review of the Structures That Facilitate Melanoma Detection. 
</details>

<details open>
    <summary>Integrated Gradients - assigns importance score to each input feature</summary>
    <img src="/uploads/b58ac916a4535ad6ac258a8d1e43517b/image.png" width="1584" height="792">
    <details>
        <summary>More examples that showcase masking of hairs and general focus of networks attention.</summary>
        <img src="/uploads/44c6b3ef2bced55465a8d869c705b555/image.png" width="1584" height="792">
        <img src="/uploads/613afbb04e596bba40c8d79946d658db/image.png" width="1584" height="792">
    </details>
</details>

<details>
    <summary>Occlusion - perturbation based replacement of rectangular regions</summary>
    <img src="/uploads/741d733287e1a5e998d851e9648a0cb1/image.png" width="1584" height="792">
</details>

<details>
    <summary>Guided backpropagation - the gradient of the target output with respect to the input</summary>
    <img src="/uploads/3f620bf6fea8bf8320c8408f955f0e4e/image.png" width="1584" height="792">
</details>


## <div align="left">Used network architectures</div>
As core model architecture EfficientNet was picked as being the most appropriate for available limited hardware resources. In the paper, the experiments were also focused on model scaling and balancing network depth, width, and resolution such that it can lead to better performance.  Additionally initial motivation of transfer learning is applicable as it is based on lack of data to train a network from scratch, lack of hardware resources and operating on low-level features (edges, blobs, textures). Therefore the ensemble includes EfficienNets pretrained on ImageNet dataset with B0 to B6 models.
[paper](https://arxiv.org/abs/1905.11946)
[code](https://github.com/lukemelas/EfficientNet-PyTorch)

## <div align="left">Notes on training process</div>
Each model was trained up to 200 epochs with decay triggered reaching epoch 75 as well as the ReduceLROnPlateau scheduler. The performance is tracked in weighted fashion to deal with class imbalance and CrossEntropyLoss is used as weight update criterion. For performance reasons CUDA's mixed precision was utilized to match necessary dynamic ranges to minimize computational demand. 
To reduce the number of uncertainties a RNG is seed and a set of Torch and CUDA benchmarks are launched at the start of each activity as a first action within entrypoint.

## <div align="left">Trained convolutional networks interpretation</div>
<details open>
    <summary>Shades of Gray color consistency</summary>
    <img src="/git_images/color_consistency.jpg">
</details>


## <div align="left">Optimizer</div>
After a short number of experiments on a partial dataset AdaBelief Optimizer was chosen. The choice is based on a fact as AdaBelief provided slightly more stable training and a small decrease in training time.
[paper](https://arxiv.org/abs/2010.07468)
[code](https://github.com/juntang-zhuang/Adabelief-Optimizer)


## <div align="left">Ensemble Learning</div>
The initial idea is to treat independent trained models as "weak classifiers" and utilize ensembling to obtain prediction with perhaps better generalized performance and reduced error. Predictions from each base model are aggregated, weighted by performance scores (on validation split) and analyzed for performance. For details please refer to the `derma_classifier` module.

<details open>
    <summary>An ensemble of networks is trained and analyzed for potential boosting</summary>
    <img src="/git_images/ensemble_performance.png" width="1190" height="712">
</details>

<details open>
    <summary>Ensemble with top 7 models (with highlighted highest scores in bold)</summary>
    <img src="/git_images/ensemble/top_selected_ensembe.jpg" width="752" height="465">
</details>

<details open>
    <summary>Ensemble with extra poor performing models (with highlighted highest scores in bold)</summary>
    <img src="/git_images/ensemble/extended_ensemble.jpg" width="756" height="650">
</details>


## <div align="left">Codebase structure</div>
This repository is structured as a monorepo and contains several modules:
 - `derma_classifier`: module that covers model training, data ingestion, experiment maintenance, performance tracker, visualisations, model interpretation and ensembling. 
 - `eda.ipynb`: Toolbox used for EDA (Exploratory Data Analysis).
 - `ml_utils`: Toolbox with generic machine learning utils to control reproducibility and data flow.
 
For more details on module usage and configuration please refer to the respective README.


## <div align="left">Installation, virtual environment and dependencies</div>
Each module within monorepo uses `poetry` to keep track of package dependencies. [poetry](https://python-poetry.org/docs/). Make sure to install poetry on your system following the official installation guide. 
In order to run the entry points and to contribute code inside each module that module should be installed by poetry and the virtual environment should be activated. For detailed instructions please refer to the respective README.


## <div align="left">License</div>
For any reuse of the software please directly contact the authors.
