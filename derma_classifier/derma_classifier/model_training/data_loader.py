import random
from collections.abc import Iterable
from pathlib import Path
from typing import Dict, List, Tuple

import albumentations as A
import cv2
import numpy as np
import pandas as pd
from albumentations.pytorch import ToTensorV2
from PIL import Image
from torch.utils.data import Dataset

from derma_classifier.model_training.config import Config
from derma_classifier.model_training.train_arguments import ExperimentParams


def sync_shuffle(a: Iterable, b: Iterable) -> Tuple[Iterable, Iterable]:
    c = list(zip(a, b))
    random.shuffle(c)
    a, b = zip(*c)
    return a, b


class DataFeeder(Dataset):
    def __init__(
        self,
        data: List[Tuple[Path, int]],
        im_size: int,
        transform: A.Compose = None,
    ):
        super(DataFeeder, self).__init__()
        self.data = data
        self.im_size = im_size
        self.transform = transform

    def __len__(self) -> int:
        return len(self.data)

    def __getitem__(self, index: int) -> Tuple[np.ndarray, int]:
        img_file, label = self.data[index]
        image = np.array(Image.open(img_file))

        if self.transform is not None:
            augmentations = self.transform(image=image)
            image = augmentations["image"]
        return image, label

    def load_image(self, index: int) -> np.ndarray:
        image = Image.open(self.data[index][0])
        resized = image.resize((self.im_size, self.im_size))
        return np.array(resized)

    def load_image_orig_shape(self, index: int) -> np.ndarray:
        image = Image.open(self.data[index][0])
        return np.array(image)


class DataManager:
    def __init__(self, cfg: Config, exp_params: ExperimentParams):
        self.cfg = cfg
        self.exp_params = exp_params
        self.data, self.im_names = self.load_data()

        self.train_data: List[Tuple[Path, int]] = []
        self.val_data: List[Tuple[Path, int]] = []
        self.test_data: List[Tuple[Path, int]] = []

        self.train_im_names: List[str] = []
        self.val_im_names: List[str] = []
        self.test_im_names: List[str] = []

        self.split_data()
        self.shuffle_data_inplace()

        if self.cfg.data_is_reduced:
            self.reduce_data()

        self.train_ds, self.val_ds, self.test_ds = self.create_feeders()

    def load_data(self) -> Tuple[List[Tuple[Path, int]], List[str]]:
        """Load data table and compose image paths"""
        gt_path = self.cfg.data.root_dir / self.cfg.data.gt_table
        self.gt_table = pd.read_csv(gt_path)
        # self.gt_table = self.gt_table.drop(columns=['Unnamed: 0'])

        data, im_names = [], []
        self.class_names = list(self.gt_table.columns)[1:]

        for index, row in self.gt_table.iterrows():
            image_filepath = self.cfg.data.image_dir / (row["image"] + ".jpg")
            label: int = list(row == 1.0).index(True) - 1
            # label = list(row[1:])
            data.append((image_filepath, label))
            im_names.append(row["image"])
        return data, im_names

    def calc_split_indices(
        self,
    ) -> Tuple[Dict[int, np.ndarray], Dict[int, Tuple[int, int]]]:
        """Caluclate indices for slicing dataset into train, val and test splits"""
        labels = np.array([x[1] for x in self.data])

        # Split data according to the config setup
        # NOTE data_split = {'train': 0.XX, 'val': 0.YY, 'test':0.ZZ}
        train = self.cfg.data_meta.data_split["train"]
        val = self.cfg.data_meta.data_split["val"]

        label_to_indices: Dict[int, np.ndarray] = {}
        label_ind_cuts: Dict[int, Tuple[int, int]] = {}

        for label in range(self.cfg.data_meta.num_classes):
            label_to_indices[label] = np.where(labels == label)[0]
            num_intances = len(label_to_indices[label])
            label_ind_cuts[label] = (
                int(num_intances * train),
                int(num_intances * (train + val)),
            )
        return label_to_indices, label_ind_cuts

    def split_data(self):
        """Split entire dataset into train, val and test parts"""
        label_to_indices, label_ind_cuts = self.calc_split_indices()

        def get_data_at(indices: List[int]) -> List[Tuple[Path, int]]:
            return [x for i, x in enumerate(self.data) if i in indices]

        def get_images_at(indices: List[int]) -> List[str]:
            return [x for i, x in enumerate(self.im_names) if i in indices]

        for lab, cut in label_ind_cuts.items():
            inds = label_to_indices[lab]

            self.train_data.extend(get_data_at(inds[: cut[0]]))
            self.val_data.extend(get_data_at(inds[cut[0] : cut[1]]))
            self.test_data.extend(get_data_at(inds[cut[1] :]))

            self.train_im_names.extend(get_images_at(inds[: cut[0]]))
            self.val_im_names.extend(get_images_at(inds[cut[0] : cut[1]]))
            self.test_im_names.extend(get_images_at(inds[cut[1] :]))

    def shuffle_data_inplace(self):
        self.train_data, self.train_im_names = sync_shuffle(
            self.train_data, self.train_im_names
        )
        self.val_data, self.val_im_names = sync_shuffle(
            self.val_data, self.val_im_names
        )
        self.test_data, self.test_im_names = sync_shuffle(
            self.test_data, self.test_im_names
        )

    def create_feeders(self) -> Tuple[DataFeeder, ...]:
        """Compose augmentation transforms and create data feeders"""
        im_size = self.exp_params.im_size
        train_ds = DataFeeder(
            self.train_data, im_size, compose_train_trf(self.exp_params)
        )
        val_ds = DataFeeder(self.val_data, im_size, compose_val_trf(self.exp_params))
        test_ds = DataFeeder(self.test_data, im_size, compose_test_trf(self.exp_params))
        return train_ds, val_ds, test_ds

    def reduce_data(self):
        """Use a small size of original data for debugging"""
        start = self.cfg.reduce_start
        n = self.cfg.reduce_num
        self.train_data = self.data[start : start + n]
        self.val_data = self.data[start : start + n]
        self.test_data = self.data[start : start + n]


def compose_train_trf(exp_params: ExperimentParams) -> A.Compose:
    return A.Compose(
        [
            A.OneOf(
                [
                    A.Resize(width=exp_params.im_size, height=exp_params.im_size),
                    A.RandomCrop(width=exp_params.im_size, height=exp_params.im_size),
                    A.CenterCrop(width=exp_params.im_size, height=exp_params.im_size),
                ],
                p=1.0,
            ),
            A.Rotate(limit=40, p=0.9, border_mode=cv2.BORDER_CONSTANT),
            A.HorizontalFlip(p=0.5),
            A.VerticalFlip(p=0.1),
            A.RGBShift(r_shift_limit=20, g_shift_limit=20, b_shift_limit=20, p=0.9),
            A.OneOf([A.Blur(blur_limit=5, p=0.5), A.ColorJitter(p=0.5)], p=1.0),
            A.CoarseDropout(
                max_holes=2, max_height=16, max_width=16, fill_value=0, p=0.5
            ),
            A.Normalize(
                mean=exp_params.color_stats.mean,
                std=exp_params.color_stats.std,
                max_pixel_value=255.0,
            ),
            ToTensorV2(),
        ]
    )


def compose_val_trf(exp_params: ExperimentParams) -> A.Compose:
    return A.Compose(
        [
            A.Resize(width=exp_params.im_size, height=exp_params.im_size),
            A.Normalize(
                mean=exp_params.color_stats.mean,
                std=exp_params.color_stats.std,
                max_pixel_value=255.0,
            ),
            ToTensorV2(),
        ]
    )


def compose_test_trf(exp_params: ExperimentParams) -> A.Compose:
    return A.Compose(
        [
            A.Normalize(
                mean=exp_params.color_stats.mean,
                std=exp_params.color_stats.std,
                max_pixel_value=255.0,
            ),
            ToTensorV2(),
        ]
    )
