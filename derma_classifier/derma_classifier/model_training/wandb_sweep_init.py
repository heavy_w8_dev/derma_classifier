import wandb


class SweepParams:
    archs = [
        "efficientnet-b0",
        "efficientnet-b1",
        "efficientnet-b2",
        "efficientnet-b3",
        "efficientnet-b4",
        "efficientnet-b5",
    ]
    batch_size = 16
    epoch = 100
    lr_rates = [1e-3]


def main():
    sweep_config = {
        "method": "grid",  # grid, random
        "metric": {"name": "loss", "goal": "minimize"},
        "parameters": {
            "arch": {"values": SweepParams.archs},
            "total_epoch": {"value": SweepParams.epoch},
            "batch_size": {"value": SweepParams.batch_size},
            "learning_rate": {"values": SweepParams.lr_rates},
        },
    }

    _ = wandb.sweep(sweep_config, project="derma_classifier")


if __name__ == "__main__":
    main()
