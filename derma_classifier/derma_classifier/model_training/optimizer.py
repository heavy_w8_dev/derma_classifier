from typing import Iterator, Union

import torch
from adabelief_pytorch import AdaBelief
from torch.optim.lr_scheduler import StepLR

from derma_classifier.model_training.train_arguments import (
    AdabeliefParams,
    AdamParams,
    AdamWParams,
    OptimizerParameters,
    SGDParams,
)

Optimizer = Union[AdaBelief, torch.optim.Adam, torch.optim.AdamW, torch.optim.SGD]


def create_optimizer(
    optim_params: OptimizerParameters, model_params: Iterator
) -> Optimizer:

    if isinstance(optim_params, SGDParams):
        return torch.optim.SGD(
            model_params,
            optim_params.lr,
            momentum=optim_params.momentum,
            weight_decay=optim_params.weight_decay,
        )
    elif isinstance(optim_params, AdamParams):
        return torch.optim.Adam(
            model_params,
            optim_params.lr,
            betas=(optim_params.beta1, optim_params.beta2),
            weight_decay=optim_params.weight_decay,
            eps=optim_params.eps,
        )
    elif isinstance(optim_params, AdamWParams):
        return torch.optim.AdamW(
            model_params,
            optim_params.lr,
            betas=(optim_params.beta1, optim_params.beta2),
            weight_decay=optim_params.weight_decay,
            eps=optim_params.eps,
        )
    elif isinstance(optim_params, AdabeliefParams):
        return AdaBelief(
            model_params,
            optim_params.lr,
            betas=(optim_params.beta1, optim_params.beta2),
            weight_decay=optim_params.weight_decay,
            eps=optim_params.eps,
            fixed_decay=False,  # from authors on ImageNet
            weight_decouple=True,  # from authors on ImageNet
            print_change_log=False,
        )
    else:
        raise TypeError(f"Cannot create requested Optimizer with {optim_params}")


def create_scheduler(optimizer: Optimizer) -> StepLR:
    return StepLR(optimizer=optimizer, step_size=25, gamma=0.5)
