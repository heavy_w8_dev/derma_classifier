import logging
from datetime import datetime
from pathlib import Path
from typing import Dict, List

import torch
from ml_utils.logger_handler import get_local_logger
from ml_utils.utils import (
    benchmark_pytorch,
    get_dataloader,
    get_writers,
    reseed,
    write_chpt,
)
from torch.utils.data import DataLoader

from derma_classifier.model_training.config import Config
from derma_classifier.model_training.data_loader import DataManager
from derma_classifier.model_training.optimizer import Optimizer
from derma_classifier.model_training.train_arguments import ExperimentParams
from derma_classifier.networks.networks import (
    Classifier,
    ModelArchitecture,
    get_model_arch_type,
)
from derma_classifier.performance.tracker import PerformanceTracker

logger = get_local_logger(__file__)


def get_datetime_tag() -> str:
    return datetime.now().strftime("%Y-%m-%d_%H_%M")


def get_experiment_tag(exp_params: ExperimentParams) -> str:
    tag = "__".join([exp_params.arch, exp_params.optimizer])
    return tag.replace("-", "_").replace(".", "_")


class ModelCheckpointStorage:
    """Keep only top cfg.keep_checkpoint_max checkpoints saved on dict"""

    def __init__(
        self,
        cp_dir: Path,
        keep_checkpoint_max: int,
        exp_params: ExperimentParams,
        class_names: List[str],
        model_arch_type: ModelArchitecture,
    ):
        self.saved_models_to_loss: Dict[Path, float] = {}
        self.keep_checkpoint_max = keep_checkpoint_max
        self.cp_dir = cp_dir
        self.exp_params = exp_params
        self.class_names = class_names
        self.model_arch_type = model_arch_type

    def remove_worst_model(self):
        """Remove worst model (one with highest loss)"""

        model_to_remove = max(
            self.saved_models_to_loss,
            key=self.saved_models_to_loss.get,  # type: ignore
        )
        try:
            logging.info(f"Removing model {model_to_remove}")
            Path.unlink(model_to_remove)
            self.saved_models_to_loss.pop(model_to_remove)
        except KeyError:
            logging.error(f"Failed to remove model from the dict {model_to_remove}")
        except FileNotFoundError:
            logging.error(f"Failed to remove the file {model_to_remove}")

    def save_checkpoint(
        self,
        model: Classifier,
        epoch: int,
        optimizer: Optimizer,
        loss: float,
    ):
        if len(self.saved_models_to_loss) > self.keep_checkpoint_max:
            self.remove_worst_model()

        save_path: Path = write_chpt(
            cp_dir=self.cp_dir,
            model=model,
            epoch=epoch,
            optimizer=optimizer,
            loss=loss,
            exp_params=self.exp_params,
            class_names=self.class_names,
            model_arch_type=self.model_arch_type,
        )
        self.saved_models_to_loss[save_path] = loss


class ExperimentWorkspace:
    def __init__(self, cfg: Config, exp_params: ExperimentParams) -> None:
        self.cfg = cfg
        self.full_tag = self.generate_experiment_tag(exp_params)

        self.log_dir = self.cfg.log.text_log_dir / self.full_tag
        self.tboard_log_dir = self.cfg.log.tboard_dir / self.full_tag
        self.cp_dir = self.cfg.checkpoint.cp_dir / "training" / self.full_tag

    def generate_experiment_tag(self, exp_params: ExperimentParams) -> str:
        time_tag = get_datetime_tag()
        exp_tag = get_experiment_tag(exp_params)
        return f"{time_tag}_{exp_tag}"

    def create_ws(self):
        workspace_dirs = [
            self.log_dir,
            self.tboard_log_dir,
            self.cp_dir,
        ]
        for d in workspace_dirs:
            d.mkdir(parents=True, exist_ok=True)


class ExperimentMaintainer:
    def __init__(self, cfg: Config, exp_params: ExperimentParams):
        self.cfg = cfg
        self.exp_params = exp_params

        self.exp_ws = ExperimentWorkspace(cfg, exp_params)
        self.exp_ws.create_ws()

        data_man = DataManager(cfg, exp_params)
        self.train_loader: DataLoader = get_dataloader(
            dataset=data_man.train_ds, cfg=cfg.hw, batch_size=exp_params.batch_size
        )
        self.val_loader: DataLoader = get_dataloader(
            dataset=data_man.val_ds, cfg=cfg.hw, batch_size=exp_params.batch_size
        )

        # Limit the number of sources of nondeterministic behavior
        reseed(self.cfg.hw.seed)
        benchmark_pytorch()

        self.writers = get_writers(self.exp_ws.tboard_log_dir)

        self.model_cp_handler = ModelCheckpointStorage(
            cp_dir=self.exp_ws.cp_dir,
            keep_checkpoint_max=self.cfg.checkpoint.keep_checkpoint_max,
            exp_params=self.exp_params,
            class_names=self.cfg.data_meta.class_names,
            model_arch_type=get_model_arch_type(exp_params.arch),
        )

        logger.info(f"Using device {self.cfg.hw.device}")

    def on_epoch_end(
        self, model: Classifier, epoch: int, optimizer: Optimizer, mean_loss: float
    ):
        """
        On end of epoch calculate performance merics, drop visuals to tensorboard,
        save checkpoint and log gpu memory to detect memory leaks.
        """
        if epoch > self.cfg.performance.eval_skip_epochs:
            pt = PerformanceTracker(
                cfg=self.cfg,
                val_loader=self.val_loader,
            )
            epoch_stats, pm = pt.compute_performance_metrics(model=model, epoch=epoch)
            pt.report_scalars(epoch, mean_loss, pm, self.writers)

            if epoch % self.cfg.performance.drop_visuals_each_epoch == 0:
                pt.report_visuals(epoch_stats, pm, self.writers)

        if (
            self.cfg.checkpoint.save_cp
            and epoch % self.cfg.checkpoint.save_cp_each_epoch == 0
        ):
            self.model_cp_handler.save_checkpoint(model, epoch, optimizer, mean_loss)

        for writer in self.writers.values():
            writer.flush()

        self.log_gpu_memory(epoch)

    def log_gpu_memory(self, epoch: int):
        """Log gpu memory to tensorboad to detect memory leaks."""
        if not torch.cuda.is_available():
            logging.info("GPU memory can not be logged, since GPU is not availible")
            return

        a = torch.cuda.memory_allocated() / (1024**2)
        b = torch.cuda.max_memory_allocated() / (1024**2)

        self.writers["gpu_memory"].add_scalars(
            main_tag="GPU memory",
            tag_scalar_dict={"mem_alloc": a, "max_mem_alloc": b},
            global_step=epoch,
        )
        logging.info(
            f"Memory footprint on epoch {epoch}: alloc {a:.2f}, max alloc {b:.2f}"
        )

    def tear_down(self):
        for writer in self.writers.values():
            writer.close()
