from pathlib import Path
from typing import Union

import torch
from dotenv import dotenv_values


class DataConfig:
    gt_table = "ISIC_2019_Training_GroundTruth.csv"
    meta_file = "ISIC_2019_Training_Metadata.csv"

    def __init__(self, data_root_dir):
        self.data_root_dir = data_root_dir
        self.root_dir = data_root_dir / "ISIC_2019_Training_Input"
        self.image_dir = self.root_dir / "data_fixed_all" / "ISIC_2019_Training_Input"


class DataMetaConfig:
    # Calculated during meta-study of dataset
    ds_mean = [0.6133, 0.5893, 0.5785]
    ds_std = [0.1703, 0.1926, 0.2017]
    data_split = {"train": 0.7, "val": 0.29, "test": 0.01}

    class_names = [
        "Melanoma",
        "Melanocytic nevus",
        "Basal cell carcinoma",
        "Actinic keratosis",
        "Benign keratosis",
        "Dermatofibroma",
        "Vascular lesion",
        "Squamous cell carcinoma",
        "None of the above",
    ]
    class_names_short = ["MEL", "MV", "BCC", "AK", "BKL", "DF", "VASC", "SCC", "UNK"]
    class_cases = [4522, 12875, 3323, 867, 2624, 239, 253, 628, 0]
    num_classes = len(class_names)

    # Sanity check
    assert len(class_names) == len(class_names_short)
    assert len(class_names) == num_classes

    # Class weights used for classwise metrics
    total_cases: int = sum(class_cases)
    # class_weights: List[float] = [x / total_cases for x in class_cases]
    class_weights = []
    for x in class_cases:
        class_weights += [x / total_cases]


class HWConfig:
    seed = 1794
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    n_workers = 4
    pin_memore = True


class LoggingConfig:
    # Text and visual logging
    tboard_is_active = True
    wandb_is_active = False
    log_step = 3

    def __init__(self, runtime_logs_root: Path):
        self.tboard_dir = runtime_logs_root / "tboard_logs"
        self.text_log_dir = runtime_logs_root / "logs"


class CheckpointConfig:
    keep_checkpoint_max = 5
    save_cp = True
    save_cp_each_epoch = 5

    def __init__(self, cp_root: Path):
        # Intermediate Checkpoints
        self.cp_dir = cp_root / "checkpoints"


class PerformanceTrackerConfig:
    # Debugging info
    drop_visuals_each_epoch = 4
    eval_skip_epochs = 0
    perf_pred_to_take = 4


class ConfigBase:
    tprofiler_is_active = False
    data_is_reduced = False

    def __init__(self, data_root_dir: Path, cp_dir: Path, runtime_logs_dir: Path):
        self.data = DataConfig(data_root_dir)
        self.data_meta = DataMetaConfig()
        self.log = LoggingConfig(runtime_logs_dir)
        self.checkpoint = CheckpointConfig(cp_dir)

        self.hw = HWConfig()
        self.performance = PerformanceTrackerConfig()


class ConfigHomelab(ConfigBase):
    data_is_reduced = False
    reduce_start = 100
    reduce_num = 16

    def __init__(self):
        module_base_dir = Path(__file__).parents[3] / "runtime_logs"
        env_file = Path(".env").resolve()
        print(f"Creating training config from env file at {env_file}")
        config = dotenv_values(env_file)
        data_root_dir = Path(config["DATA_ROOT_DIR"])
        super().__init__(data_root_dir, module_base_dir, module_base_dir)


class ConfigKaggle(ConfigBase):
    def __init__(self):
        module_base_dir = Path(__file__).parents[3] / "runtime_logs"
        data_root_dir = Path("/kaggle/input/isic-2019-preprocessed/")
        super().__init__(data_root_dir, module_base_dir, module_base_dir)


class ConfigDebug(ConfigHomelab):
    # Reduce dataset size for faster debugging
    data_is_reduced = True
    reduce_start = 100
    reduce_num = 4

    # Configuration of PyTorch Profiler to detect performance bottlenecks
    tprofiler_is_active = True
    profiling_duration = 20  # batches
    n_epochs_to_profile = 0

    def __init__(self):
        super().__init__()
        module_base_dir = Path(__file__).parents[3] / "runtime_logs"
        self.tboard_profiler_dir = module_base_dir / "tboard_profiler_logs"


Config = Union[ConfigHomelab, ConfigKaggle, ConfigDebug]
