from typing import List, Optional

import torch
import torch.nn as nn
import wandb
from ml_utils.logger_handler import get_local_logger
from tqdm import tqdm

from derma_classifier.model_training.config import Config
from derma_classifier.model_training.experiment_maintainer import ExperimentMaintainer
from derma_classifier.model_training.optimizer import (
    Optimizer,
    create_optimizer,
    create_scheduler,
)
from derma_classifier.model_training.train_arguments import (
    ExperimentParams,
    OptimizerParameters,
)
from derma_classifier.networks.networks import Classifier, create_model, train_network

logger = get_local_logger(__file__)


class ModelTrainer:
    def __init__(
        self,
        exp_params: ExperimentParams,
        opt_params: OptimizerParameters,
        cfg: Config,
        model: Optional[Classifier] = None,
    ):
        self.exp_params = exp_params
        self.opt_params = opt_params
        self.cfg = cfg
        self.model = model or create_model(
            device=cfg.hw.device,
            num_classes=cfg.data_meta.num_classes,
            arch=exp_params.arch,
        )

        self.em = ExperimentMaintainer(self.cfg, self.exp_params)
        self.scaler = torch.cuda.amp.GradScaler()
        self.optimizer: Optimizer = create_optimizer(
            self.opt_params, self.model.parameters()
        )

        self.lr_scheduler = create_scheduler(self.optimizer)
        self.loss_fn = nn.CrossEntropyLoss(
            weight=torch.tensor(
                self.cfg.data_meta.class_weights,
                device=self.cfg.hw.device,
                dtype=torch.float,
            )
        )

        if self.cfg.log.wandb_is_active:
            wandb.watch(self.model)

    def train_fn(self, train_model: Classifier, t_profiler=None) -> float:
        loop = tqdm(self.em.train_loader)
        losses: List[float] = []

        for data, targets in loop:
            data = data.to(device=self.cfg.hw.device)
            targets = targets.to(device=self.cfg.hw.device)

            # forward propagation
            with torch.cuda.amp.autocast():
                scores = train_model(data)
                loss = self.loss_fn(scores, targets.long())

            loss_value = loss.item()
            losses.append(loss_value)

            # backward propagation
            self.optimizer.zero_grad()
            self.scaler.scale(loss).backward()
            self.scaler.step(self.optimizer)
            self.scaler.update()

            # Update torch profiler
            if t_profiler:
                t_profiler.step()

            # update tqdm progress bar
            loop.set_postfix(loss=loss_value)

        self.lr_scheduler.step()
        return sum(losses) / len(losses)

    def train(self):
        """Run train function and report results per epoch"""
        with train_network(self.model) as train_model:
            for epoch in range(self.exp_params.total_epoch):
                mean_loss = self.train_fn(train_model)
                self.em.on_epoch_end(train_model, epoch, self.optimizer, mean_loss)
            self.em.tear_down()

    def profile_network_training(self):
        """Run train function and profile model operator, device activity and stack trace"""
        try:
            import torch.profiler as profiler
        except ImportError as e:
            logger.error("Failed to import Torch profiler")
            logger.error(e)

        self.cfg.tboard_profiler_dir.mkdir(parents=True, exist_ok=True)

        with train_network(self.model) as train_model:
            profiler_schedule = profiler.schedule(
                wait=0, warmup=0, active=self.cfg.profiling_duration, repeat=2
            )
            tboard_trace = profiler.tensorboard_trace_handler(
                self.cfg.tboard_profiler_dir
            )

            for _ in range(self.cfg.n_epochs_to_profile):
                with profiler.profile(
                    schedule=profiler_schedule,
                    record_shapes=True,
                    profile_memory=True,
                    with_stack=True,
                    with_flops=True,
                    on_trace_ready=tboard_trace,
                ) as pr:
                    _ = self.train_fn(train_model, pr)
            self.em.tear_down()


def main():
    from derma_classifier.model_training.config import ConfigHomelab
    from derma_classifier.model_training.train_arguments import parse_parameters

    cfg = ConfigHomelab()
    exp_params, opt_params = parse_parameters()

    m_trainer = ModelTrainer(exp_params, opt_params, cfg)
    m_trainer.train()


if __name__ == "__main__":
    main()
