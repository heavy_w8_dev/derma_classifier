import argparse
from pathlib import Path
from typing import Any, Optional

import wandb
from dotenv import dotenv_values

from derma_classifier.model_training.config import Config, ConfigHomelab, ConfigKaggle
from derma_classifier.model_training.model_trainer import ModelTrainer
from derma_classifier.model_training.train_arguments import (
    AdabeliefParams,
    ExperimentParams,
)
from derma_classifier.model_training.wandb_sweep_init import SweepParams


def get_train_config(env_file: Optional[Path] = Path(".env")) -> Config:
    env_cfg = dotenv_values(env_file)
    cfg: Config = (
        ConfigKaggle() if env_cfg["DERMA_TRAIN_ENV"] == "Kaggle" else ConfigHomelab()
    )
    cfg.log.wandb_is_active = True
    return cfg


def conduct_experiment(train_config: Config, wandb_config: Any):
    exp_params = ExperimentParams(
        arch=wandb_config.arch,
        total_epoch=wandb_config.total_epoch,
        batch_size=wandb_config.batch_size,
    )
    opt_params = AdabeliefParams(lr=wandb_config.learning_rate)

    m_trainer = ModelTrainer(
        exp_params=exp_params,
        opt_params=opt_params,
        cfg=train_config,
    )
    m_trainer.train()


def train_wandb():
    # Default values for hyper-parameters to sweep over
    config_defaults = {
        "arch": SweepParams.archs[0],
        "batch_size": SweepParams.batch_size,
        "learning_rate": SweepParams.lr_rates[0],
        "total_epoch": SweepParams.epoch,
    }

    # Initialize a new wandb run
    wandb.init(config=config_defaults)
    wandb_config = wandb.config
    train_config = get_train_config()
    conduct_experiment(train_config, wandb_config)


def main():

    parser = argparse.ArgumentParser(description="Launch wandb agent with provided id")
    parser.add_argument("--sweep_id", type=str, help="wandb sweep id")
    args = parser.parse_args()
    sweep_id = args.sweep_id

    print(f"Launching wandb agent with sweep id = {sweep_id}")
    wandb.agent(
        sweep_id=sweep_id,
        function=train_wandb,
        entity="uladzislau",
        project="derma_classifier",
        count=3,
    )


if __name__ == "__main__":
    main()
