import argparse
from abc import ABC
from dataclasses import dataclass, field
from typing import List, Tuple, Union


@dataclass
class DatasetColorStats:
    mean: List[float] = field(default_factory=lambda: [0.6133, 0.5893, 0.5785])
    std: List[float] = field(default_factory=lambda: [0.1703, 0.1926, 0.2017])


@dataclass
class ExperimentParams:
    arch: str = "efficientnet-b0"
    optimizer: str = "adabelief"
    total_epoch: int = 100
    im_size: int = 300
    batch_size: int = 32
    color_stats: DatasetColorStats = field(default_factory=DatasetColorStats)


@dataclass
class OptimizerParamsABC(ABC):
    optimizer: str
    eps: float = 1e-16
    lr: float = 1e-3
    weight_decay: float = 5e-4


@dataclass
class SGDParams(OptimizerParamsABC):
    optimizer: str = "SGD"
    momentum: float = 0.9


@dataclass
class AdamParams(OptimizerParamsABC):
    optimizer: str = "Adam"
    beta1: float = 0.9
    beta2: float = 0.999


@dataclass
class AdamWParams(OptimizerParamsABC):
    optimizer: str = "AdamW"
    beta1: float = 0.9
    beta2: float = 0.999


@dataclass
class AdabeliefParams(OptimizerParamsABC):
    optimizer: str = "adabelief"
    beta1: float = 0.9  # from authors on ImageNet
    beta2: float = 0.999  # from authors on ImageNet
    print_change_log: bool = False
    weight_decay: float = 1e-2  # from authors on ImageNet


OptimizerParameters = Union[SGDParams, AdamParams, AdamWParams, AdabeliefParams]


def parse_optim_params(args):
    optimizer = args.optimizer
    if optimizer == "sgd":
        opt_params = SGDParams(
            lr=args.lr,
            weight_decay=args.weight_decay,
            eps=args.eps,
            momentum=args.momentum,
        )
    elif optimizer == "adam":
        opt_params = AdamParams(
            lr=args.lr,
            weight_decay=args.weight_decay,
            eps=args.eps,
            beta1=args.beta1,
            beta2=args.beta2,
        )
    elif optimizer == "adamw":
        opt_params = AdamWParams(
            lr=args.lr,
            weight_decay=args.weight_decay,
            eps=args.eps,
            beta1=args.beta1,
            beta2=args.beta2,
        )
    elif optimizer == "adabelief":
        opt_params = AdabeliefParams(
            lr=args.lr,
            weight_decay=args.weight_decay,
            eps=args.eps,
            beta1=args.beta1,
            beta2=args.beta2,
        )
    else:
        raise TypeError(f"Provided Optimizer {optimizer} is of unknown type")

    return opt_params


def match_model_to_image_size(model: str) -> int:
    arch_to_im_size = {
        "efficientnet-b0": 224,
        "efficientnet-b1": 240,
        "efficientnet-b2": 260,
        "efficientnet-b3": 300,
        "efficientnet-b4": 380,
        "efficientnet-b5": 456,
        "efficientnet-b6": 528,
    }
    return arch_to_im_size[model]


def parse_parameters() -> Tuple[ExperimentParams, OptimizerParameters]:
    parser = argparse.ArgumentParser(description="Derma_Classifier Training launcher")
    # Experiment parameters
    parser.add_argument(
        "--arch", default="efficientnet-b0", type=str, help="model architecture"
    )
    parser.add_argument(
        "--optimizer",
        default="adabelief",
        type=str,
        help="optimizer",
        choices=["sgd", "adam", "adamw", "adabelief"],
    )
    parser.add_argument(
        "--total_epoch", default=100, type=int, help="Total number of training epochs"
    )
    parser.add_argument("--batch_size", type=int, default=4, help="batch size")

    # Common optimizer parameters
    parser.add_argument("--lr", default=0.001, type=float, help="learning rate")
    parser.add_argument("--eps", default=1e-16, type=float, help="eps for var adam")
    parser.add_argument(
        "--weight_decay", default=5e-4, type=float, help="weight decay for optimizers"
    )

    # Distinctive optimizer parameters
    parser.add_argument("--momentum", default=0.9, type=float, help="momentum term")
    parser.add_argument(
        "--beta1", default=0.9, type=float, help="Adam coefficients beta_1"
    )
    parser.add_argument(
        "--beta2", default=0.999, type=float, help="Adam coefficients beta_2"
    )

    args = parser.parse_args()

    exp_params = ExperimentParams(
        arch=args.arch,
        optimizer=args.optimizer,
        total_epoch=args.total_epoch,
        batch_size=args.batch_size,
        im_size=match_model_to_image_size(args.arch),
    )

    opt_params = parse_optim_params(args)

    return exp_params, opt_params
