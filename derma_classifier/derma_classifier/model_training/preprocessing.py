from typing import Dict

import cv2
import numpy as np
from matplotlib import pyplot as plt
from ml_utils.albumentations.color_constancy_alb import shade_of_gray_cc
from ml_utils.albumentations.lens_cropping import crop_out_lens

from derma_classifier.visualization.vis_utils import (
    get_image_and_patches_plot,
    plot_to_image,
)


def get_image_patches(
    orig_image: np.ndarray, patch_size=(256, 256)
) -> Dict[str, np.ndarray]:

    orig_h, orig_w, c = orig_image.shape
    ph, pw = patch_size

    if orig_h < ph or orig_w < pw:
        raise IndexError("Image dim is smaller that requested patch")

    dim = (int(2 * ph), int(2 * pw))
    image = cv2.resize(orig_image, dim, interpolation=cv2.INTER_AREA)

    h, w, c = image.shape

    # Center crop corner coordinates
    hc = int((h - ph) / 2)
    wc = int((w - pw) / 2)

    return {
        "tl": image[:ph, :pw, :],
        "tr": image[:ph:, w - pw : w, :],
        "bl": image[h - ph : h, :pw, :],
        "br": image[h - ph : h, w - pw : w, :],
        "center": image[hc : hc + ph, wc : wc + pw, :],
    }


def test_patching():
    image_path = "data/testo.jpg"
    img = cv2.imread(image_path, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, (512, 512))

    patches = get_image_patches(img)
    figure = get_image_and_patches_plot(cv2.resize(img, (256, 256)), patches)

    combined_image = plot_to_image(figure)
    cv2.imwrite("patches.jpg", combined_image)
    cv2.imwrite("original.jpg", img)


def test_zoom(img):
    return crop_out_lens(img)


def main():

    image_path = "data/zoom_me.jpg"
    image_path = "data/testo.jpg"
    img = cv2.imread(image_path)
    img = cv2.resize(img, (512, 512))

    # test_zoom(img)

    plt.plot(),
    plt.xticks([]), plt.yticks([])

    cc = shade_of_gray_cc(img)
    dbg_im = np.hstack((img, cc))

    plt.imshow(cv2.cvtColor(dbg_im, cv2.COLOR_RGB2BGR), vmin=0, vmax=255)
    plt.show()


if __name__ == "__main__":
    main()
