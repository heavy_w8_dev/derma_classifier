import datetime
from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Dict, List, Tuple

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import torch
from captum.attr import (
    GuidedBackprop,
    GuidedGradCam,
    IntegratedGradients,
    NoiseTunnel,
    Occlusion,
)
from captum.attr import visualization as viz
from matplotlib.colors import LinearSegmentedColormap
from torch.utils.tensorboard import SummaryWriter

from derma_classifier.networks.networks import Classifier
from derma_classifier.visualization.vis_utils import drop_figure_to_writer


@dataclass
class ModelInterpretationData:
    image_as_batch: torch.Tensor
    pred_label: int
    gt_label: str


def get_experiment_tag() -> str:
    return datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f")


class ModelInterpreter:
    def __init__(
        self,
        model: Classifier,
        last_layer,
        class_names: List[str],
        tboard_log_dir: Path,
    ):
        self.model = model
        self.last_layer = last_layer
        self.class_names = class_names

        self.writer_to_callable = self.get_writer_to_callable()
        self.writers = self.create_tboard_writers(
            tboard_log_dir / get_experiment_tag(), self.writer_to_callable.keys()
        )

    def get_writer_to_callable(self):
        return {
            "intergrated_grad": self.get_integrated_grads,
            "guided_grad_cam": self.get_guided_grad_cam,
            "guided_backprop": self.get_guided_backprop,
            "occlusion": self.get_occlusion,
            "noise_tunnel": self.get_noise_tunnel,
        }

    def create_tboard_writers(
        self, tboard_log_dir, writers: List[str]
    ) -> Dict[str, SummaryWriter]:
        for w in writers:
            (tboard_log_dir / w).mkdir(parents=True, exist_ok=True)

        return {w: SummaryWriter(tboard_log_dir / w, max_queue=1000) for w in writers}

    def calculate_attributes(
        self, label_to_interpreter_data: Dict[str, ModelInterpretationData]
    ):

        for writer_name, calc_func in self.writer_to_callable.items():
            for idx, interpreter_data in enumerate(label_to_interpreter_data.values()):
                print(f"\nComputing {writer_name} for {interpreter_data.gt_label}")
                figure = self.compute_attrs_and_visualize(
                    interpreter_data=interpreter_data, attr_calc_func=calc_func
                )
                drop_figure_to_writer(
                    figure=figure,
                    tboard_writer=self.writers[writer_name],
                    tag=writer_name,
                    epoch=idx,
                )

                # NOTE depending on parameters attributions calculation might hang
                # While trying to allocalate memory on both CPU and GPU
                # Solution - Fast flush signal propagation
                for writer in self.writers.values():
                    writer.flush()

    def compute_attrs_and_visualize(
        self, interpreter_data: ModelInterpretationData, attr_calc_func: Callable
    ) -> plt.Figure:
        """
        Compute attributes per provided attr_calc_func for each possible class.
        Return a plot including original image and attributions per class as a grid.
        """
        cols = 1 + len(self.class_names) // 2
        fig, axs = plt.subplots(2, cols, figsize=(24, 12))
        gt_label = self.class_names.index(interpreter_data.gt_label)
        image_batch = interpreter_data.image_as_batch

        self.visualize_original_image(image_batch, plt_fig_axis=(fig, axs[0][0]))

        for n, fake_label in enumerate(self.class_names):
            print(f"Calculation activation for {fake_label}")
            # Where to put on image grid
            grid_pos = (0, n + 1) if n < cols - 1 else (1, n + 1 - cols)
            figure_title = f"Activation for {fake_label} \nCorrect label is {self.class_names[gt_label]}"
            fake_label_idx = self.class_names.index(fake_label)
            attribute = attr_calc_func(image_batch, fake_label_idx)

            self.visualize_attribute(
                attribute=attribute,
                image_batch=image_batch,
                plt_fig_axis=(fig, axs[grid_pos[0]][grid_pos[1]]),
                figure_title=figure_title,
            )
        plt.tight_layout()
        return fig

    def visualize_original_image(
        self,
        image_batch: torch.Tensor,
        plt_fig_axis: Tuple[plt.Figure, mpl.axes.Axes],
    ):
        image = np.transpose(image_batch.squeeze().cpu().detach().numpy(), (1, 2, 0))
        _ = viz.visualize_image_attr(
            attr=image,
            original_image=image,
            plt_fig_axis=plt_fig_axis,
            method="original_image",
        )

    def visualize_attribute(
        self,
        image_batch: torch.Tensor,
        attribute: torch.Tensor,
        plt_fig_axis: Tuple[plt.Figure, mpl.axes.Axes],
        figure_title: str,
    ):
        default_cmap = LinearSegmentedColormap.from_list(
            name="yellowish blue",
            colors=[(0, "#FFFFFF"), (0.25, "#0000FF"), (1, "#0000FF")],
            N=256,
        )
        # Unpack single image from tensor and shuffle axis
        attr = np.transpose(attribute[0].squeeze().cpu().detach().numpy(), (1, 2, 0))
        image = np.transpose(image_batch[0].squeeze().cpu().detach().numpy(), (1, 2, 0))
        _ = viz.visualize_image_attr(
            attr=attr,
            original_image=image,
            plt_fig_axis=plt_fig_axis,
            cmap=default_cmap,
            sign="positive",
            title=figure_title,
        )

    def get_integrated_grads(
        self, images: torch.Tensor, predicted_label: int
    ) -> torch.Tensor:
        integrated_gradients = IntegratedGradients(self.model)
        return integrated_gradients.attribute(
            images, target=predicted_label, internal_batch_size=2, n_steps=100
        )

    def get_guided_backprop(
        self, images: torch.Tensor, predicted_label: int
    ) -> torch.Tensor:
        guided_bp = GuidedBackprop(self.model)
        return guided_bp.attribute(images, target=predicted_label)

    def get_guided_grad_cam(
        self, images: torch.Tensor, predicted_label: int
    ) -> torch.Tensor:
        guided_gc = GuidedGradCam(self.model, self.last_layer)
        return guided_gc.attribute(images, target=predicted_label)

    def get_noise_tunnel(
        self, images: torch.Tensor, predicted_label: int
    ) -> torch.Tensor:
        integrated_gradients = GuidedBackprop(self.model)
        noise_tunnel = NoiseTunnel(integrated_gradients)
        return noise_tunnel.attribute(
            images,
            target=predicted_label,
            nt_samples=10,
            nt_samples_batch_size=5,
            nt_type="smoothgrad",
        )

    def get_occlusion(self, images: torch.Tensor, predicted_label: int) -> torch.Tensor:
        occlusion = Occlusion(self.model)
        return occlusion.attribute(
            images,
            target=predicted_label,
            strides=(3, 3, 3),
            sliding_window_shapes=(3, 5, 5),
        )
