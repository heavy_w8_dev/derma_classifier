import argparse
from pathlib import Path
from typing import Any, Dict, List

import torch
import torch.nn.functional as F
from ml_utils.utils import benchmark_pytorch, reseed

from derma_classifier.model_interpretation.load_utils import (
    get_val_image_loader,
    load_model,
    take_best_models,
)
from derma_classifier.model_interpretation.model_interpreter import (
    ModelInterpretationData,
    ModelInterpreter,
)
from derma_classifier.model_training.config import Config, ConfigHomelab
from derma_classifier.networks.networks import Classifier, ModelArchitecture


def extract_examples_by_class(
    cfg: Config,
    checkpoint: Dict[str, Any],
    model: Classifier,
) -> Dict[str, ModelInterpretationData]:
    """
    Iterate over validaion dataloader and extract every label type if predicted correctly.
    """
    class_names: List[str] = checkpoint["class_names"]
    checkpoint["batch_size"] = 1  # iterate over single image
    loader = get_val_image_loader(cfg, checkpoint)

    label_to_interpreter_data = {}

    for label_to_take in class_names:
        for data, targets in loader:
            images = data.to(cfg.hw.device)
            labels = targets.to(cfg.hw.device)

            gt_label = labels[0].item()
            image = images[0]
            image.requires_grad = True
            image_as_batch = image.unsqueeze(0)

            output = model(image_as_batch)
            output = F.softmax(output, dim=1)

            _, pred_label = torch.topk(output, 1)
            pred_label.squeeze_()
            pred_label = pred_label.item()

            if (
                gt_label == pred_label
                and class_names.index(label_to_take) == pred_label
            ):
                label_to_interpreter_data[label_to_take] = ModelInterpretationData(
                    image_as_batch=image_as_batch,
                    pred_label=pred_label,
                    gt_label=label_to_take,
                )
                print(f"Label taken {label_to_take}")
                break
        break
    return label_to_interpreter_data


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Derma_Classifier Model Interpretation"
    )
    parser.add_argument("--checkpoints_root", type=str, help="model checkpoint root")
    parser.add_argument("--log_root_dir", type=str, help="path to put logs root")
    return parser.parse_args()


def get_last_layer(model, arch: ModelArchitecture):
    if arch == ModelArchitecture.EFFICIENT_NET:
        return model._conv_head
    elif arch == ModelArchitecture.RESNET:
        return model.avgpool
    else:
        raise ValueError(f"Invalid Model Architecturet choice: {arch}")


def main():
    cfg = ConfigHomelab()
    args = parse_arguments()

    log_dir = Path(args.log_root_dir)
    log_dir.mkdir(parents=True, exist_ok=True)

    # Limit the number of sources of nondeterministic behavior
    reseed(cfg.hw.seed)
    benchmark_pytorch()

    checkpoint_root = Path(args.checkpoints_root).resolve(strict=True)
    chosen_models = take_best_models(checkpoint_root)

    for checkpoint_path in chosen_models:
        checkpoint = torch.load(checkpoint_path, map_location=cfg.hw.device)
        model = load_model(checkpoint, cfg)
        last_layer = get_last_layer(model=model, arch=checkpoint["model_arch_type"])

        mi = ModelInterpreter(
            model=model,
            last_layer=last_layer,
            class_names=checkpoint["class_names"],
            tboard_log_dir=log_dir / checkpoint_path.parent.name,
        )

        label_to_interpreter_data = extract_examples_by_class(
            cfg=cfg,
            checkpoint=checkpoint,
            model=model,
        )
        mi.calculate_attributes(label_to_interpreter_data)


if __name__ == "__main__":
    main()
