from pathlib import Path
from typing import Any, Dict, List

from ml_utils.utils import get_dataloader
from torch.utils.data import DataLoader

from derma_classifier.model_training.config import Config
from derma_classifier.model_training.data_loader import DataManager
from derma_classifier.model_training.train_arguments import ExperimentParams
from derma_classifier.networks.networks import create_model


def get_val_image_loader(cfg: Config, checkpoint: Dict[str, Any]) -> DataLoader:
    exp_params = ExperimentParams(
        im_size=checkpoint["im_size"],
        batch_size=checkpoint["batch_size"],
        color_stats=checkpoint["color_stats"],
    )
    data_man = DataManager(cfg, exp_params)
    return get_dataloader(
        dataset=data_man.val_ds, cfg=cfg.hw, batch_size=exp_params.batch_size
    )


def load_model(checkpoint: Dict[str, Any], cfg: Config):
    model = create_model(
        device=cfg.hw.device,
        num_classes=cfg.data_meta.num_classes,
        arch=checkpoint["model_arch"],
    )
    model.load_state_dict(checkpoint["model_state_dict"])
    model.eval()
    return model


def take_best_models(checkpoints_dir: Path) -> List[Path]:
    """From each experiment take a checkpoint with lowest loss value"""
    get_checkpoint_loss = lambda x: x.stem.split("loss_", 1)[1]
    best_models_filepaths = []
    for exp_dir in checkpoints_dir.iterdir():
        checkpoint_filename = min(exp_dir.iterdir(), key=get_checkpoint_loss)
        print(f"Best model from {exp_dir.stem} is {checkpoint_filename.stem}")
        best_models_filepaths.append(checkpoint_filename)
    return best_models_filepaths
