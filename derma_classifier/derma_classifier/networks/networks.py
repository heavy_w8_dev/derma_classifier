from contextlib import contextmanager
from enum import Enum
from typing import Union

import torch
from efficientnet_pytorch import EfficientNet as EN_pretrained
from efficientnet_pytorch.model import EfficientNet
from torchvision.models import resnet34
from torchvision.models.resnet import ResNet

from derma_classifier.networks.alex_net import AlexNet

Classifier = Union[AlexNet, EfficientNet, ResNet]


class ModelArchitecture(Enum):
    EFFICIENT_NET = 1
    RESNET = 2
    AlexNet = 3


@contextmanager
def eval_network(network: Classifier):
    was_training = network.training

    try:
        network.eval()
        yield network
    finally:
        if was_training:
            network.train()


@contextmanager
def train_network(network: Classifier):
    was_training = network.training

    try:
        network.train()
        yield network
    finally:
        if not was_training:
            network.eval()


def get_Alexnet(num_classes: int) -> AlexNet:
    return AlexNet(num_classes)


def get_ResNet(num_classes: int) -> ResNet:
    model = resnet34(pretrained=True)
    # Replace original fc with task-specific
    num_ftrs = model.fc.in_features
    model.fc = torch.nn.Linear(num_ftrs, num_classes)
    return model


def get_EfficientNet(num_classes: int, name="efficientnet-b3") -> EfficientNet:
    return EN_pretrained.from_pretrained(model_name=name, num_classes=num_classes)


def create_model(device: torch.device, num_classes: int, arch: str) -> Classifier:
    if arch.startswith("efficientnet"):
        model = get_EfficientNet(num_classes, arch)
    elif arch.startswith("resnet"):
        model = get_ResNet(num_classes)
    elif arch.startswith("AlexNet"):
        model = get_Alexnet(num_classes)
    else:
        raise TypeError(f"Cannot create a model for requested arch {arch}")

    return model.to(device)


def get_model_arch_type(arch: str) -> ModelArchitecture:
    if arch.startswith("efficientnet"):
        return ModelArchitecture.EFFICIENT_NET
    elif arch.startswith("resnet"):
        return ModelArchitecture.RESNET
    elif arch.startswith("AlexNet"):
        return ModelArchitecture.AlexNet
    else:
        raise TypeError(f"Cannot create a model for requested arch {arch}")
