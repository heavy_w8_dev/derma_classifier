import io
import itertools
from typing import Dict, List

import cv2
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import PIL
import torch
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms

matplotlib.use("Agg")

iter_colors = itertools.cycle(
    [
        "aqua",
        "darkorange",
        "cornflowerblue",
        "khaki",
        "purple",
        "plum",
        "sienna",
        "tan",
        "teal",
        "olive",
        "turquoise",
        "tomato",
        "crimson",
        "orange",
        "lime",
    ]
)


def create_sample_predictions_figure(
    images: List[np.ndarray],
    true_labels: List[int],
    pred_labels: List[int],
    class_names: List[str],
) -> matplotlib.figure.Figure:
    """Returns a matplotlib figure with sample image, gt and network guesses."""
    image_num_to_rc = {
        4: (2, 2),
        8: (4, 2),
        6: (3, 2),
        9: (3, 3),
        16: (4, 4),
    }
    n_rows, n_cols = image_num_to_rc[len(images)]
    figure, axs = plt.subplots(
        n_rows, n_cols, sharex=True, sharey=True, figsize=(20, 14)
    )

    for i in range(n_rows):
        for j in range(n_cols):
            idx = i + j * n_rows
            image = images[idx]
            # true_label_idx = np.argmax(labels[idx])
            true_label = str(class_names[true_labels[idx]])
            pred_label = str(class_names[pred_labels[idx]])
            axs[i, j].label_outer()
            axs[i, j].imshow(image)
            title = f"g_truth - {true_label}" + ", \n " + "pred- " + pred_label
            color = "green" if pred_label == true_label else "red"
            axs[i, j].set_title(title, color=color)
    plt.tight_layout()
    return figure


def create_conf_mat_figure(
    cm: np.ndarray, class_names: List[str]
) -> matplotlib.figure.Figure:
    """Returns a matplotlib figure with plotted confusion matrix."""
    figure = plt.figure()
    plt.imshow(cm, interpolation="nearest", cmap=plt.cm.Blues)
    plt.title("Confusion matrix")
    plt.colorbar()

    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names)
    plt.yticks(tick_marks, class_names)

    # Normalize the confusion matrix.
    cm = np.around(cm.astype("float") / cm.sum(axis=1)[:, np.newaxis], decimals=2)

    # Use white text if squares are dark; otherwise black.
    # threshold = cm.max() / 2.0
    color = "green"
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        # color = "white" if cm[i, j] > threshold else "black"
        plt.text(
            j,
            i,
            cm[i, j],
            horizontalalignment="center",
            verticalalignment="center",
            color=color,
        )

    # plt.tight_layout()
    plt.ylabel("True label")
    plt.xlabel("Predicted label")
    plt.xticks(rotation=45)
    return figure


def create_roc_figure(
    fpr: Dict[int, np.ndarray],
    tpr: Dict[int, np.ndarray],
    roc_auc: Dict[int, float],
    class_names: List[str],
) -> matplotlib.figure.Figure:
    """Returns a matplotlib figure with plotted Receiver Operating Characteristic."""

    figure = plt.figure(figsize=(12, 8))

    for i, (class_name, color) in enumerate(zip(class_names, iter_colors)):
        tag = f"ROC curve for {class_name} (area = {roc_auc[i]:0.2f})"
        plt.plot(fpr[i], tpr[i], color=color, lw=2, label=tag)

    plt.plot([0, 1], [0, 1], "k--", lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("Receiver operating characteristic")
    plt.legend(loc="lower right")
    return figure


def create_pr_figure(
    precision: Dict[int, np.ndarray],
    recall: Dict[int, np.ndarray],
    average_precision: Dict[int, float],
    class_names: List[str],
) -> matplotlib.figure.Figure:
    """Returns a matplotlib figure with plotted Precision-Recall Curve."""

    figure = plt.figure(figsize=(12, 8))
    for i, (class_name, color) in enumerate(zip(class_names, iter_colors)):
        tag = f"PR curve for {class_name} (avg_prec = {average_precision[i]:0.2f})"
        plt.plot(
            recall[i],
            precision[i],
            color=color,
            lw=2,
            label=tag,
        )

    plt.plot([0, 1], [0, 1], "k--", lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.title("Precision-Recall curve")
    plt.legend(loc="lower right")
    return figure


def get_image_and_patches_plot(
    image: np.ndarray, patches: Dict[str, np.ndarray]
) -> matplotlib.figure.Figure:
    """Returns a matplotlib figure with and extracted patches."""

    n_rows, n_cols = 2, 3
    images = [image] + list(patches.values())
    names = ["original"] + list(patches.keys())
    figure, axs = plt.subplots(
        n_rows, n_cols, sharex=True, sharey=True, figsize=(20, 14)
    )

    for i, j in itertools.product(range(n_rows), range(n_cols)):
        idx = i + j * n_rows
        image_to_show = images[idx]
        axs[i, j].label_outer()
        axs[i, j].imshow(image_to_show)
        title = names[idx]
        axs[i, j].set_title(title)
    plt.tight_layout()
    return figure


def draw_cam_on_image(img: np.ndarray, cam_img: np.ndarray) -> np.ndarray:
    """Overlay the image with CAM image."""
    h, w, c = img.shape
    cam_positive = cam_img - np.min(cam_img)
    cam_normalized = cam_positive / np.max(cam_positive)  # Normalize between 0-1
    cam = cv2.resize(cam_normalized, (w, h))

    heatmap = cv2.applyColorMap(np.uint8(255 * cam), cv2.COLORMAP_JET)
    im_with_cam = np.float32(heatmap) + np.float32(img)
    im_with_cam_normalized = im_with_cam / np.max(im_with_cam)
    return np.array(255 * im_with_cam_normalized, dtype=np.uint8)


def plot_to_image(fig: matplotlib.figure.Figure) -> np.ndarray:
    buf = io.BytesIO()
    fig.savefig(buf)
    buf.seek(0)
    pil_im = PIL.Image.open(buf)
    cv2_im = np.array(pil_im)
    plt.close("all")
    return cv2_im[:, :, :3]


def image_to_tensor(image: np.ndarray) -> torch.Tensor:
    return transforms.ToTensor()(image)


def tensor_to_image(tensor: torch.Tensor) -> np.ndarray:
    pil_im = transforms.ToPILImage()(tensor.squeeze(0))
    return np.array(pil_im)


def drop_figure_to_writer(
    figure: matplotlib.figure.Figure, tboard_writer: SummaryWriter, tag: str, epoch: int
):
    image = plot_to_image(figure)
    tboard_writer.add_image(tag, image_to_tensor(image), epoch)
