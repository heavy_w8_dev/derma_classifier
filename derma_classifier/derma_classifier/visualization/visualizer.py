from typing import Dict, List

import matplotlib
import numpy as np
import wandb
from torch.utils.tensorboard import SummaryWriter

from derma_classifier.model_training.config import Config
from derma_classifier.performance.metrics import PerformanceMetrics
from derma_classifier.performance.tracker import EpochStatistics
from derma_classifier.visualization.vis_utils import (
    create_conf_mat_figure,
    create_pr_figure,
    create_roc_figure,
    create_sample_predictions_figure,
    drop_figure_to_writer,
)


class PerformanceVisualizer:
    def __init__(
        self,
        cfg: Config,
        epoch_stats: EpochStatistics,
        performance_metrics: PerformanceMetrics,
        writers: Dict[str, SummaryWriter],
    ):
        self.cfg = cfg
        self.pm = performance_metrics
        self.epoch_stats = epoch_stats
        self.writers = writers

        self.plot_id_to_plots: Dict[str, matplotlib.figure.Figure] = {}
        self.plot_id_to_tag: Dict[str, str] = {
            "pr_curve": "Precision-Recall curve",
            "roc_curve": "Receiver operating characteristic",
            "predicts": "Prediction",
            "conf_mat": "Confusion matrix",
        }

    def create_plots(self, images: List[np.ndarray]):
        self.create_pred_plot(images)
        self.create_conf_mat_plot()
        self.create_pr_plot()
        self.create_roc_plot()

    def create_pred_plot(self, images: List[np.ndarray]):
        fig = create_sample_predictions_figure(
            images=images,
            true_labels=self.epoch_stats.true_labels,
            pred_labels=self.epoch_stats.pred_labels,
            class_names=self.cfg.data_meta.class_names,
        )
        self.plot_id_to_plots["predicts"] = fig

    def create_conf_mat_plot(self):
        fig = create_conf_mat_figure(
            cm=self.pm.weighted.confusion_mat,
            class_names=self.cfg.data_meta.class_names_short,
        )
        self.plot_id_to_plots["conf_mat"] = fig

    def create_pr_plot(self):
        fig = create_pr_figure(
            precision=self.pm.classwise.precision_thr,
            recall=self.pm.classwise.recall_thr,
            average_precision=self.pm.classwise.avg_prec,
            class_names=self.cfg.data_meta.class_names,
        )
        self.plot_id_to_plots["pr_curve"] = fig

    def create_roc_plot(self):
        fig = create_roc_figure(
            fpr=self.pm.classwise.fpr,
            tpr=self.pm.classwise.tpr,
            roc_auc=self.pm.classwise.roc_auc,
            class_names=self.cfg.data_meta.class_names,
        )
        self.plot_id_to_plots["roc_curve"] = fig

    def drop_visuals_to_tboard(self):
        for plot_id, fig in self.plot_id_to_plots.items():
            drop_figure_to_writer(
                figure=fig,
                tboard_writer=self.writers[plot_id],
                epoch=self.epoch_stats.epoch,
                tag=self.plot_id_to_tag[plot_id],
            )

    def drop_visuals_to_wandb(self):
        plot_data = {
            self.plot_id_to_tag[plot_id]: fig
            for plot_id, fig in self.plot_id_to_plots.items()
        }
        wandb.log(plot_data, step=self.epoch_stats.epoch)
