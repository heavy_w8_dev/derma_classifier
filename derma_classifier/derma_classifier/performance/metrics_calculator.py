import warnings
from typing import Dict, List, Tuple

import numpy as np
import torch
from ml_utils.logger_handler import get_local_logger
from sklearn.metrics import (
    auc,
    average_precision_score,
    balanced_accuracy_score,
    confusion_matrix,
    precision_recall_curve,
    precision_score,
    recall_score,
    roc_curve,
)
from sklearn.preprocessing import label_binarize

from derma_classifier.performance.metrics import (
    ClasswiseMetrics,
    GuessesMetrics,
    WeightedMetrics,
)
from derma_classifier.performance.tracker import EpochStatistics

warnings.filterwarnings("ignore")  # avoid sklearn messages for small DS

logger = get_local_logger(__file__)


class PerformanceMetricsCalculator:
    def __init__(
        self,
        epoch_stats: EpochStatistics,
        n_classes: int,
        sample_weight: List[float],
    ):
        self.epoch_stats = epoch_stats
        self.n_classes = n_classes
        self.sample_weight = sample_weight

    def compute_accuracy(self) -> GuessesMetrics:
        n_guessed: torch.Tensor = torch.zeros(1, dtype=torch.int32)
        for pred, gt in zip(self.epoch_stats.pred_labels, self.epoch_stats.true_labels):
            n_guessed += pred == gt
        return GuessesMetrics(
            n_guessed=int(n_guessed),
            accuracy=float(n_guessed / len(self.epoch_stats.true_labels)),
        )

    def compute_weighted_metrics(self) -> WeightedMetrics:
        gt, preds = self.epoch_stats.true_labels, self.epoch_stats.pred_labels
        weights = [self.sample_weight[int(x)] for x in gt]

        confusion_mat = confusion_matrix(gt, preds)
        precision = precision_score(
            gt, preds, average="weighted", sample_weight=weights
        )
        recall = recall_score(gt, preds, average="weighted", sample_weight=weights)
        f1_score = 2 * (precision * recall) / (precision + recall)
        bal_acc = balanced_accuracy_score(gt, preds, sample_weight=weights)

        return WeightedMetrics(
            f1_score=f1_score,
            bal_acc=bal_acc,
            precision=precision,
            recall=recall,
            confusion_mat=confusion_mat,
        )

    def get_binarized_labels(self) -> Tuple[np.ndarray, np.ndarray]:
        """Get predicted and groundtruth labels in a one-vs-all fashion"""
        if self.n_classes == 2:
            gt = [(1 - label, label) for label in self.epoch_stats.true_labels]
            true_labels = np.array(gt)

            preds = [(1 - label, label) for label in self.epoch_stats.pred_labels]
            pred_labels = np.array(preds)
        else:
            classes = list(range(self.n_classes))
            true_labels = label_binarize(self.epoch_stats.true_labels, classes=classes)
            pred_labels = label_binarize(self.epoch_stats.pred_labels, classes=classes)

        return pred_labels, true_labels

    def compute_classwise_metrics(self) -> ClasswiseMetrics:
        pred_labels, true_labels = self.get_binarized_labels()
        probs = np.array([x.data.numpy() for x in self.epoch_stats.pred_probs])

        roc_auc: Dict[int, float] = {}
        avg_prec: Dict[int, float] = {}
        precision_thr: Dict[int, np.ndarray] = {}
        recall_thr: Dict[int, np.ndarray] = {}
        fpr: Dict[int, np.ndarray] = {}
        tpr: Dict[int, np.ndarray] = {}

        for i in range(self.n_classes):
            y_true, y_prob = true_labels[:, i], probs[:, i]
            class_w = [self.sample_weight[int(x)] for x in y_true]

            try:
                (
                    precision_thr[i],
                    recall_thr[i],
                    _,
                ) = precision_recall_curve(y_true, y_prob, sample_weight=class_w)
                avg_prec[i] = average_precision_score(
                    y_true, y_prob, sample_weight=class_w
                )
                fpr[i], tpr[i], _ = roc_curve(y_true, y_prob, sample_weight=class_w)
                roc_auc[i] = auc(fpr[i], tpr[i])

            except ValueError:
                logger.error(f"np.isnan(y_true).any() {np.isnan(y_true).any()}")
                logger.error(f"np.isnan(y_prob).any() {np.isnan(y_prob).any()}")
                logger.error(
                    f"np.isnan(np.array(class_w)).any() {np.isnan(np.array(class_w)).any()}"
                )

        return ClasswiseMetrics(
            roc_auc=roc_auc,
            avg_prec=avg_prec,
            precision_thr=precision_thr,
            recall_thr=recall_thr,
            fpr=fpr,
            tpr=tpr,
        )
