from typing import Any, Dict, List, Tuple

import torch
import torch.nn as nn
import wandb
from ml_utils.logger_handler import get_local_logger
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from derma_classifier.model_training.config import Config
from derma_classifier.model_training.data_loader import DataFeeder
from derma_classifier.networks.networks import Classifier, eval_network
from derma_classifier.performance.metrics import EpochStatistics, PerformanceMetrics
from derma_classifier.performance.metrics_calculator import PerformanceMetricsCalculator
from derma_classifier.visualization.visualizer import PerformanceVisualizer

logger = get_local_logger(__file__)


class PerformanceTracker:
    def __init__(
        self,
        cfg: Config,
        val_loader: DataLoader,
    ):
        self.cfg = cfg
        self.loader = val_loader
        self.ds: DataFeeder = self.loader.dataset  # type: ignore

        self.log_sm = torch.nn.LogSoftmax(dim=0)
        self.loss_fn = nn.CrossEntropyLoss(
            weight=torch.tensor(
                self.cfg.data_meta.class_weights,
                device=self.cfg.hw.device,
                dtype=torch.float,
            )
        )

    def valid_run(self, eval_model: Classifier, epoch: int) -> EpochStatistics:
        batch_losses: List[float] = []
        true_labels: List[int] = []
        pred_labels: List[int] = []
        pred_probs: List[torch.Tensor] = []

        with torch.no_grad():
            for input_tensor, targets in self.loader:
                input_tensor = input_tensor.to(device=self.cfg.hw.device)
                targets = targets.to(device=self.cfg.hw.device)

                with torch.cuda.amp.autocast():
                    output = eval_model(input_tensor)
                    loss = self.loss_fn(output, targets.long())

                loss_value = loss.item()
                batch_losses.append(loss_value)

                class_probs = [self.log_sm(el).cpu() for el in output]
                _, class_preds = torch.max(output, 1)

                pred_probs += class_probs
                pred_labels += class_preds.tolist()
                true_labels += targets.tolist()

        return EpochStatistics(
            pred_labels=pred_labels,
            pred_probs=pred_probs,
            true_labels=true_labels,
            valid_mean_loss=sum(batch_losses) / len(batch_losses),
            epoch=epoch,
        )

    def compute_performance_metrics(
        self,
        model: Classifier,
        epoch: int,
    ) -> Tuple[EpochStatistics, PerformanceMetrics]:

        with eval_network(model) as eval_model:
            epoch_stats = self.valid_run(eval_model, epoch)

        pm_calc = PerformanceMetricsCalculator(
            epoch_stats=epoch_stats,
            n_classes=self.cfg.data_meta.num_classes,
            sample_weight=self.cfg.data_meta.class_weights,
        )

        pm = PerformanceMetrics(
            valid_mean_loss=epoch_stats.valid_mean_loss,
            guessed=pm_calc.compute_accuracy(),
            weighted=pm_calc.compute_weighted_metrics(),
            classwise=pm_calc.compute_classwise_metrics(),
        )
        return epoch_stats, pm

    def report_visuals(
        self,
        epoch_stats: EpochStatistics,
        pm: PerformanceMetrics,
        writers: Dict[str, SummaryWriter],
    ):
        vis = PerformanceVisualizer(self.cfg, epoch_stats, pm, writers)
        images = [
            self.ds.load_image(i) for i in range(self.cfg.performance.perf_pred_to_take)
        ]
        vis.create_plots(images)

        if self.cfg.log.tboard_is_active:
            vis.drop_visuals_to_tboard()
        if self.cfg.log.wandb_is_active:
            vis.drop_visuals_to_wandb()

    def report_scalars(
        self,
        epoch: int,
        train_mean_loss: float,
        pm: PerformanceMetrics,
        writers: Dict[str, SummaryWriter],
    ):
        # Aggregate metrics
        tag_to_scalar: Dict[str, Any] = {
            "Validation / Accuracy": pm.guessed.accuracy,
            "Validation / Balanced Accuracy score": pm.weighted.bal_acc,
            "Validation / F1 score": pm.weighted.f1_score,
            "Validation / Precision score": pm.weighted.precision,
            "Validation / Recall score": pm.weighted.recall,
            "Validation / Mean Loss": pm.valid_mean_loss,
            "Training / Mean Loss": train_mean_loss,
        }

        # Add per-class metrics
        for i, class_name in enumerate(self.cfg.data_meta.class_names):
            tag_to_scalar[f"ROC AUC / {class_name}"] = pm.classwise.roc_auc[i]
            tag_to_scalar[
                f"Avg Precision score / {class_name}"
            ] = pm.classwise.avg_prec[i]

        print(
            f"End of epoch {epoch}: Guessed {pm.guessed.n_guessed} / {len(self.ds)}\
            with accuracy {pm.guessed.accuracy:.2f} and f1 {pm.weighted.f1_score}"
        )

        if self.cfg.log.tboard_is_active:
            self.report_scalars_tboard(tag_to_scalar, epoch, writers["scalar"])

        if self.cfg.log.wandb_is_active:
            self.report_scalars_wandb(tag_to_scalar, epoch)

    def report_scalars_tboard(
        self, tag_to_scalar: Dict[str, Any], epoch: int, writer: SummaryWriter
    ):
        for tag, value in tag_to_scalar.items():
            writer.add_scalar(tag, value, epoch)

    def report_scalars_wandb(self, tag_to_scalar: Dict[str, Any], epoch: int):
        wandb.log(tag_to_scalar, step=epoch)
