from dataclasses import dataclass
from typing import Dict, List

import numpy as np
import torch


@dataclass
class EpochStatistics:
    pred_labels: List[int]
    pred_probs: List[torch.Tensor]
    true_labels: List[int]
    valid_mean_loss: float
    epoch: int


@dataclass
class GuessesMetrics:
    accuracy: float
    n_guessed: int


@dataclass
class WeightedMetrics:
    bal_acc: float
    confusion_mat: np.ndarray
    f1_score: float
    precision: float
    recall: float


@dataclass
class ClasswiseMetrics:
    avg_prec: Dict[int, float]
    fpr: Dict[int, np.ndarray]
    precision_thr: Dict[int, np.ndarray]
    recall_thr: Dict[int, np.ndarray]
    roc_auc: Dict[int, float]
    tpr: Dict[int, np.ndarray]


@dataclass
class PerformanceMetrics:
    classwise: ClasswiseMetrics
    guessed: GuessesMetrics
    valid_mean_loss: float
    weighted: WeightedMetrics
