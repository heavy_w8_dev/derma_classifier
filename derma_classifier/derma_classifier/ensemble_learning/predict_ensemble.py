from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import pandas as pd
from tqdm import tqdm

from derma_classifier.ensemble_learning.utils import (
    drop_classwise_classification_report,
    get_classwise_metrics,
)
from derma_classifier.model_training.config import Config


def predict_ensemble(
    model_to_df: Dict[str, pd.DataFrame], weights: pd.DataFrame, class_names: List[str]
) -> pd.DataFrame:

    first_df = next(iter(model_to_df.values()))
    result_rows: List[Tuple] = []

    for idx, row in tqdm(first_df.iterrows(), total=first_df.shape[0]):
        stacked = []
        for model, predictions in model_to_df.items():
            w = weights.loc[weights["model_name"] == model, class_names].values[0]
            p = predictions.loc[idx, class_names].values
            stacked.append(w * p)

        stacked_array = np.array(stacked)  # type: ignore
        stacked_array = np.where(stacked_array != -np.inf, stacked_array, None)  # type: ignore
        stacked_array = stacked_array.astype(np.float32)  # type: ignore
        sum_per_class = np.nansum(stacked_array, axis=0)  # type: ignore
        sum_per_class = np.where(sum_per_class != 0, sum_per_class, -np.inf)  # type: ignore

        result_rows.append(
            (
                row["img_file"],
                row["ground_truth"],
                *sum_per_class,
            )
        )

    return pd.DataFrame(
        data=result_rows,
        columns=first_df.columns,
    )


def combine_predictions(cfg: Config, predictions_dir: Path, dir_with_weights: Path):
    model_to_df = {}

    for pred_filepath in predictions_dir.glob("*.csv"):
        df = pd.read_csv(pred_filepath, index_col=False)
        model_to_df[pred_filepath.stem] = df

    for weights_filepath in dir_with_weights.glob("*.csv"):
        weights = pd.read_csv(weights_filepath, index_col=False)
        pred_df = predict_ensemble(model_to_df, weights, cfg.data_meta.class_names)

        clf_report = get_classwise_metrics(
            df=pred_df, class_names=cfg.data_meta.class_names
        )
        drop_classwise_classification_report(
            clf_report=clf_report,
            storage_dir=dir_with_weights,
            filename_to_match=weights_filepath.name,
        )
