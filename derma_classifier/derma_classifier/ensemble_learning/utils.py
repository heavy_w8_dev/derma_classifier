import json
from pathlib import Path
from typing import Dict, List

import numpy as np
import pandas as pd
from sklearn.metrics import classification_report

ClassificationReport = Dict[str, Dict[str, float]]

CLASSWISE_REPORT = "classwise_classification_report"
AVG_REPORT = "avg_classification_report"


def get_classwise_metrics(
    df: pd.DataFrame, class_names: List[str]
) -> ClassificationReport:

    y_true = df["ground_truth"].values
    probas = df[class_names].values
    y_pred = probas.argmax(axis=1)

    unique_pred = np.unique(y_pred)
    cr = classification_report(
        y_true=y_true,
        y_pred=y_pred,
        labels=unique_pred,
        target_names=[class_names[x] for x in unique_pred],
        output_dict=True,
    )
    print(pd.DataFrame(cr).transpose())
    return cr


def get_matched_classwise_classification_report_path(
    storage_dir: Path, filename_to_match: str
) -> Path:
    cr_dir = storage_dir / CLASSWISE_REPORT
    cr_dir.mkdir(parents=True, exist_ok=True)
    cr_filename = f"{filename_to_match}_classwise"
    return Path(cr_dir / cr_filename).with_suffix(".json")


def get_matched_avg_classification_report_path(
    storage_dir: Path, filename_to_match: str
) -> Path:
    cr_dir = storage_dir / AVG_REPORT
    cr_dir.mkdir(parents=True, exist_ok=True)
    cr_filename = f"{filename_to_match}_avg"
    return Path(cr_dir / cr_filename).with_suffix(".json")


def drop_classwise_classification_report(
    clf_report: ClassificationReport, storage_dir: Path, filename_to_match: str
):
    cr_filepath = get_matched_classwise_classification_report_path(
        storage_dir=storage_dir, filename_to_match=filename_to_match
    )
    with open(cr_filepath, "w") as output_file:
        json.dump(clf_report, output_file, indent=4)
    print(f"Classification report is written to {cr_filepath}")
