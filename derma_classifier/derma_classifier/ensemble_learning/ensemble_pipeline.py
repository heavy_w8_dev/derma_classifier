import json
from pathlib import Path

import pandas as pd
from ml_utils.utils import benchmark_pytorch, reseed

from derma_classifier.ensemble_learning.predict_ensemble import combine_predictions
from derma_classifier.ensemble_learning.predict_per_model import predict_per_model
from derma_classifier.ensemble_learning.utils import CLASSWISE_REPORT
from derma_classifier.ensemble_learning.weights_calculator import (
    calculate_avg_weights,
    calculate_classwise_weights,
)
from derma_classifier.model_training.config import ConfigHomelab


class ExperimentWorkspace:
    project_root = Path(__file__).parents[3].resolve(strict=True)
    runtime_logs = (project_root / "runtime_logs/").resolve(strict=True)
    checkpoints_root = (runtime_logs / "checkpoints").resolve(strict=True)

    ensemble_runtime_dir = runtime_logs / "ensemble"
    predictions_dir = ensemble_runtime_dir / "per_model_predictions"
    classwise_weights_dir = ensemble_runtime_dir / "classwise_ensemble_weights"
    avg_weights_dir = ensemble_runtime_dir / "avg_ensemble_weights"

    comparison_csv = ensemble_runtime_dir / "comparison.csv"

    def construct_workspace(self):
        dirs = (
            self.ensemble_runtime_dir,
            self.predictions_dir,
            self.classwise_weights_dir,
            self.avg_weights_dir,
        )

        for d in dirs:
            d.mkdir(parents=True, exist_ok=True)


def export_comparison_table(workspace: ExperimentWorkspace):
    """
    Write comparison table with performance scores for single models and ensembles.
    """
    df = pd.DataFrame()
    dirs = (
        workspace.predictions_dir,
        workspace.classwise_weights_dir,
        workspace.avg_weights_dir,
    )
    for d in dirs:
        report_dir = d / CLASSWISE_REPORT

        for filepath in report_dir.glob("*.json"):
            with open(filepath, "r") as input_file:
                scores = json.load(input_file)["weighted avg"]

            model_name = f"{d.stem}_{filepath.stem}"
            for score_name in ("precision", "recall", "f1-score"):
                df.loc[model_name, score_name] = scores[score_name]

    print(df)
    df.to_csv(workspace.comparison_csv, index=True)


def main():
    ws = ExperimentWorkspace()
    ws.construct_workspace()
    cfg = ConfigHomelab()

    # Limit the number of sources of nondeterministic behavior
    reseed(cfg.hw.seed)
    benchmark_pytorch()

    predict_per_model(
        cfg=cfg,
        checkpoints_root=ws.checkpoints_root,
        predictions_dir=ws.predictions_dir,
    )
    calculate_classwise_weights(
        cfg=cfg,
        predictions_dir=ws.predictions_dir,
        dir_with_weights=ws.classwise_weights_dir,
    )
    calculate_avg_weights(
        cfg=cfg,
        predictions_dir=ws.predictions_dir,
        dir_with_weights=ws.avg_weights_dir,
    )
    combine_predictions(
        cfg=cfg,
        predictions_dir=ws.predictions_dir,
        dir_with_weights=ws.classwise_weights_dir,
    )
    combine_predictions(
        cfg=cfg,
        predictions_dir=ws.predictions_dir,
        dir_with_weights=ws.avg_weights_dir,
    )

    export_comparison_table(workspace=ws)


if __name__ == "__main__":
    main()
