import json
from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List

import pandas as pd
import torch

from derma_classifier.ensemble_learning.utils import (
    drop_classwise_classification_report,
    get_classwise_metrics,
    get_matched_avg_classification_report_path,
)
from derma_classifier.model_interpretation.load_utils import (
    get_val_image_loader,
    load_model,
    take_best_models,
)
from derma_classifier.model_training.config import Config
from derma_classifier.performance.metrics import EpochStatistics, PerformanceMetrics
from derma_classifier.performance.tracker import PerformanceTracker


@dataclass
class PredictionData:
    epoch_stats: EpochStatistics
    perf_metrics: PerformanceMetrics
    img_files: List[str]
    class_names: List[str]

    def to_predictions_df(self) -> pd.DataFrame:
        columns = ["img_file", "ground_truth"] + self.class_names
        rows = (
            (x, y, *z.tolist())
            for x, y, z in zip(
                self.img_files,
                self.epoch_stats.true_labels,
                self.epoch_stats.pred_probs,
            )
        )
        return (
            pd.DataFrame(
                data=rows,
                columns=columns,
            )
            .sort_values(by=["img_file"])
            .reset_index(drop=True)
        )

    def to_metrics(self) -> Dict[str, float]:
        return {
            "Accuracy": self.perf_metrics.guessed.accuracy,
            "Balanced Accuracy score": self.perf_metrics.weighted.bal_acc,
            "F1 score": self.perf_metrics.weighted.f1_score,
            "Precision score": self.perf_metrics.weighted.precision,
            "Recall score": self.perf_metrics.weighted.recall,
            "Mean Loss": self.perf_metrics.valid_mean_loss,
            "Num guessed": self.perf_metrics.guessed.n_guessed,
        }


class ModelPredictionPipeline:
    def __init__(
        self,
        cfg: Config,
        checkpoint_path: Path,
        predictions_dir: Path,
    ):
        self.cfg = cfg
        self.checkpoint_path = checkpoint_path
        self.predictions_dir = predictions_dir

        self.checkpoint = torch.load(self.checkpoint_path, map_location=cfg.hw.device)
        self.model_basename = self.checkpoint_path.parent.stem.replace("-", "_")

    def run_model(self):
        """Run model over data from validation split, save predictions and scores"""
        pred_data = self.get_prediction_data()
        self.save_predictions(pred_data)
        self.save_avg_classification_report(pred_data)
        self.save_classwise_classification_report(pred_data)

    def get_prediction_data(self) -> PredictionData:
        """
        Iterate over validaion dataloader and extract class probabilities and ground truth.
        """
        loader = get_val_image_loader(self.cfg, self.checkpoint)
        model = load_model(self.checkpoint, self.cfg)

        pt = PerformanceTracker(cfg=self.cfg, val_loader=loader)
        epoch_stats, perf_metrics = pt.compute_performance_metrics(model=model, epoch=0)
        img_files = [x[0].stem for x in loader.dataset.data]  # type: ignore

        return PredictionData(
            epoch_stats=epoch_stats,
            perf_metrics=perf_metrics,
            img_files=img_files,
            class_names=self.checkpoint["class_names"],
        )

    def save_predictions(self, pred_data: PredictionData):
        """Write prediction to a csv"""
        pred_df = pred_data.to_predictions_df()
        csv_filepath = (self.predictions_dir / self.model_basename).with_suffix(".csv")
        pred_df.to_csv(csv_filepath, index=False)
        print(f"CSV with prediction results is saved to {csv_filepath}")

    def save_avg_classification_report(self, pred_data: PredictionData):
        """Write averaged scores (over entire validation split) into json"""
        json_filepath = get_matched_avg_classification_report_path(
            storage_dir=self.predictions_dir,
            filename_to_match=self.model_basename,
        )
        with open(json_filepath, "w") as output_file:
            json.dump(pred_data.to_metrics(), output_file, indent=4)
        print(f"JSON with averaged metrics results is saved to {json_filepath}")
        print(pred_data.to_metrics())

    def save_classwise_classification_report(self, pred_data: PredictionData):
        """Write classwise scores (over entire validation split) into json"""
        pred_df = pred_data.to_predictions_df()
        clf_report = get_classwise_metrics(
            df=pred_df, class_names=pred_data.class_names
        )
        drop_classwise_classification_report(
            clf_report=clf_report,
            storage_dir=self.predictions_dir,
            filename_to_match=self.model_basename,
        )


def predict_per_model(
    cfg: Config,
    checkpoints_root: Path,
    predictions_dir: Path,
):
    chosen_models = take_best_models(checkpoints_root)
    for checkpoint_path in chosen_models:
        mp = ModelPredictionPipeline(
            cfg=cfg,
            checkpoint_path=checkpoint_path,
            predictions_dir=predictions_dir,
        )
        mp.run_model()
