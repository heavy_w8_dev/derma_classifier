import json
from pathlib import Path

import numpy as np
import pandas as pd

from derma_classifier.ensemble_learning.utils import (
    get_matched_avg_classification_report_path,
    get_matched_classwise_classification_report_path,
)
from derma_classifier.model_training.config import Config

EMPTY_SCORE = np.inf  # used to be multiplied by strictly negative log-softmaxed scores


class WeightsCalculator:
    def __init__(self, cfg: Config, prediction_csv_root: Path):
        self.cfg = cfg
        self.class_names = self.cfg.data_meta.class_names
        self.n_classes = len(self.class_names)

        self.pred_to_w_cr_paths = {
            pred_filepath: get_matched_classwise_classification_report_path(
                storage_dir=prediction_csv_root,
                filename_to_match=pred_filepath.stem,
            )
            for pred_filepath in prediction_csv_root.glob("*.csv")
        }

        self.pred_to_avg_cr_paths = {
            pred_filepath: get_matched_avg_classification_report_path(
                storage_dir=prediction_csv_root,
                filename_to_match=pred_filepath.stem,
            )
            for pred_filepath in prediction_csv_root.glob("*.csv")
        }
        self.n_models = len(self.pred_to_w_cr_paths)

    def create_blank_df(self) -> pd.DataFrame:
        """Create a blank DataFrame with models as rows and classes as columns"""
        df = pd.DataFrame(
            columns=self.class_names,
            index=list(range(self.n_models)),
        )
        df["model_name"] = [x.stem for x in self.pred_to_w_cr_paths]
        return df

    def get_equal_weights(self) -> pd.DataFrame:
        """
        Compose DataFrame where all models have the same score for all classes.
        """
        df = self.create_blank_df()
        w = 1 / self.n_models
        weights = [w] * self.n_classes
        for id in range(self.n_models):
            df.loc[id, self.class_names] = weights
        return df

    def get_weighted_by_score(self, score_name: str) -> pd.DataFrame:
        """
        Compose DataFrame where each model have desired score for each class.
        The score is taken from averaged scores (over entire validation split).
        """
        df = self.create_blank_df()
        for id, cr_filepath in enumerate(self.pred_to_avg_cr_paths.values()):
            with open(cr_filepath, "r") as input_file:
                clf_report = json.load(input_file)
            score = [clf_report.get(score_name, EMPTY_SCORE)]
            df.loc[id, self.class_names] = score * self.n_classes
        return df

    def get_weighted_by_class_and_score(self, score_name: str) -> pd.DataFrame:
        """
        Compose DataFrame where each model have desired score for each class.
        The score is taken from classwise scores (over entire validation split).
        """
        df = self.create_blank_df()
        for id, cr_filepath in enumerate(self.pred_to_w_cr_paths.values()):
            with open(cr_filepath, "r") as input_file:
                clf_report = json.load(input_file)
            scores_per_class = [clf_report.get(label, {}) for label in self.class_names]
            scores = [
                scores.get(score_name, EMPTY_SCORE) for scores in scores_per_class
            ]
            scores = [
                s if s != 0 else EMPTY_SCORE for s in scores
            ]  # sklearn empty classes

            df.loc[id, self.class_names] = scores
        return df


def save_df(df: pd.DataFrame, directory: Path, filename_stem: str):
    csv_filepath = directory / f"{filename_stem}.csv"
    df.to_csv(csv_filepath, index=False)
    print(f"CSV with weights is saved to {csv_filepath}")


def calculate_classwise_weights(
    cfg: Config, predictions_dir: Path, dir_with_weights: Path
):
    wcalc = WeightsCalculator(cfg, predictions_dir)

    score_to_stem = {
        "f1-score": "weighted_by_f1",
        "precision": "weighted_by_precision",
        "recall": "weighted_by_recall",
    }

    for score, df_stem in score_to_stem.items():
        df = wcalc.get_weighted_by_class_and_score(score_name=score)
        save_df(df, dir_with_weights, df_stem)

    df_average = wcalc.get_equal_weights()
    save_df(df_average, dir_with_weights, "weighted_equal")


def calculate_avg_weights(cfg: Config, predictions_dir: Path, dir_with_weights: Path):
    wcalc = WeightsCalculator(cfg, predictions_dir)
    score_to_stem = {
        "Accuracy": "avg_accuracy",
        "Balanced Accuracy score": "avg_bal_acc_score",
        "F1 score": "avg_f1",
        "Precision score": "avg_prec_score",
        "Recall score": "avg_recall",
        "Mean Loss": "avg_mean_loss",
        "Num guessed": "avg_num_guessed",
    }

    for score, df_stem in score_to_stem.items():
        df = wcalc.get_weighted_by_score(score_name=score)
        save_df(df, dir_with_weights, df_stem)
