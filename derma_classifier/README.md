# Module that covers model training, data ingestion, experiment maintenance, performance tracker, visualisations, model interpretation and ensembling.

## Installation, virtual environment and dependencies
Each module within monorepo uses poetry  to keep track of package dependencies. [poetry](https://python-poetry.org/docs/).
If not yet installed, install poetry on your system following the official installation guide. It is crucial to follow the recommended installation procedure at the project website.
In order to run the entry point the module should be installed by poetry and virtual environment should be activated.

As per poetry standard there is a configuration file pyproject.toml that specifies all repository dependencies and dev tool.
```bash
cd PATH_TO_MONOREPO_ROOT/derma_classifier
poetry install
# or to ignore dev dependencies
poetry install --no-dev
```
To activate the shell after installation:
```bash
poetry shell
```

## Codebase structure
This module is structured as a monorepo and contains several componetns:
 - `ensemble_learning` : Ensemble learning pipeline - extract single model predictions, calculate classwise and avg weight, combine predictions and export comparison table
 - `model_interpretation` : Calculate a set of gradient-based and layer-based attributes and visualise in tensorboard
 - `networks` : Definition of Deep Neural Networks Architectures and utils to parse them
 - `model_training` : Model trainer for single models, driver code for training an ensemble of models using Weight and Biases sweeps, data preprocessing and parsing tool
 - `performance` : Model performance tracker and runtime metrics calculator
 - `visualization` : Toolbox with capability to generate performance plots and drop them to tensorboard at the end of each epoch
 - `derma_classifier`: module that covers model training, data ingestion, experiment maintenance, performance tracker, visualisations, model interpretation and ensembling.

The file `.env` used to specify data location and should be located at the module top level directory, alongside `pyproject.toml` and `poetry.lock` files.
```bash
export DERMA_TRAIN_ENV="Homelab"
export DATA_ROOT_DIR="/home/vlad/Data"
```

## Entry points
```bash
cd PATH_TO_MONOREPO_ROOT/derma_classifier
poetry shell

# train a single model based on CLI input
python3 derma_classifier/model_training/model_trainer.py

# train an ensemble of models using Weight and Biases sweeps
python3 derma_classifier/model_training/wandb_sweep_init.py
python3 derma_classifier/model_training/wandb_agent.py

# calculate a set of gradient-based and layer-based attributes and visualise in tensorboard
python3 derma_classifier/model_interpretation/interpret_model.py

# Ensemble learning pipeline - extract single model predictions, calculate classwise and avg weight
# combine predictions and export comparison table
python3 derma_classifier/ensemble_learning/ensemble_pipeline.py
```
