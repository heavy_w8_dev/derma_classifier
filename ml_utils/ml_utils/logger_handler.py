import logging
from pathlib import Path

HIGH_LEVEL_LOGGER_NAME = "Derma Classifier app"


def get_local_logger(file):
    logger = logging.getLogger(f"{HIGH_LEVEL_LOGGER_NAME}.{Path(file).stem}")
    logger.setLevel(logging.DEBUG)
    return logger


def setup_logger(logger_name: str) -> logging.Logger:
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)
    sh.setFormatter(formatter)
    logger.addHandler(sh)
    return logger
