import cv2
import numpy as np
from albumentations.core.transforms_interface import ImageOnlyTransform


class LensCropping(ImageOnlyTransform):
    """Zoom in to image and supress black area prevalence if intensity does not change much.

    Args:
        diff_intensity (int): max intensity change (ratio) to discard cropping

    Targets:
        image

    Image types:
        uint8, float32
    """

    def __init__(self, diff_intensity=0.05, always_apply=True, p=1.0):
        super(LensCropping, self).__init__(always_apply, p)
        self.diff_intensity = diff_intensity

    def apply(self, image, diff_intensity=0, **params):
        return crop_out_lens(image, diff_intensity)

    def get_params(self):
        return {
            "diff_intensity": self.diff_intensity,
        }

    def get_transform_init_args_names(self):
        return "diff_intensity"


def crop_out_lens(image, diff_intensity):
    try:
        bw = binarize(image)
        filled = fill_holes(bw)
        x, y, w, h = get_bbox_limits(filled)

        roi = image[y : y + h, x : x + w, :]

        # Check whether the intensity differs
        ratios = image.sum(axis=0).sum(axis=0) / roi.sum(axis=0).sum(axis=0)

        if np.all(np.abs(1 - ratios) < diff_intensity):
            result = roi
        else:
            # mean intensity inside bbox differs
            result = image
    except cv2.error:
        result = image

    return result


def fill_holes(im_th):
    # Mask used to flood filling.
    # NOTE size needs to be 2 pixels larger
    im_floodfill = im_th.copy()
    h, w = im_th.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)

    cv2.floodFill(im_floodfill, mask, (0, 0), 255)
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)

    # Return selected foreground
    return im_th | im_floodfill_inv


def binarize(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
    return thresh


def get_bbox_limits(bw):
    contours, hierarchy = cv2.findContours(bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[
        -2:
    ]

    if len(contours) == 1:
        cnt = contours[0]
    else:
        areas = [cv2.contourArea(c) for c in contours]
        max_index = np.argmax(areas)
        cnt = contours[max_index]

    x, y, w, h = cv2.boundingRect(cnt)
    return x, y, w, h
