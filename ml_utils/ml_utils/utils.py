import logging
import random
from typing import Dict, Any, List
import numpy as np
import torch
from pathlib import Path
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter


def get_dataloader(dataset, cfg, batch_size: int) -> DataLoader:
    return DataLoader(
        dataset,
        batch_size=batch_size,
        num_workers=cfg.n_workers,
        pin_memory=cfg.pin_memore,
        shuffle=False,
    )


def get_writers(tboard_log_dir: Path) -> Dict[str, SummaryWriter]:
    writers = [
        "gpu_memory",
        "scalar",
        "predicts",
        "conf_mat",
        "gradcam",
        "pr_curve",
        "roc_curve",
    ]
    return {w: SummaryWriter(tboard_log_dir / w, max_queue=1000) for w in writers}


def write_chpt(
    cp_dir: Path,
    model,
    epoch: int,
    optimizer,
    loss: float,
    exp_params: Any,
    class_names: List[str],
    model_arch_type: Any,
) -> Path:

    save_path = cp_dir / f"epoch_{epoch}_loss_{loss:.5f}.pth"
    torch.save(
        {
            "epoch": epoch,
            "model_state_dict": model.state_dict(),
            "optimizer_state_dict": optimizer.state_dict(),
            "loss": loss,
            "im_size": exp_params.im_size,
            "batch_size": exp_params.batch_size,
            "color_stats": exp_params.color_stats,
            "class_names": class_names,
            "model_arch": exp_params.arch,
            "model_arch_type": model_arch_type,
        },
        save_path,
    )
    logging.debug(f"Checkpoint {epoch} saved !")
    return save_path


def benchmark_pytorch():
    if torch.cuda.is_available():
        # enables benchmark mode in cudnn
        torch.backends.cudnn.benchmark = True
        # warns about NaN or infinity gradients when True.
        torch.autograd.set_detect_anomaly(False)
        # tells the time spent for each operation on CPU and GPU when True.
        torch.autograd.profiler.profile(False)
        # creates an annotated timeline for NVIDIA Visual Profiler (NVP) when True.
        torch.autograd.profiler.emit_nvtx(False)


def reseed(seed: int):
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
